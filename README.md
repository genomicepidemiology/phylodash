# PhyloDash

Dashboard for phylogeny visualisation consisting of map, metadata table, timeline with heatmap and phylogeny tree.

## Example

[covid phylogeny](https://www.covid19dataportal.org/phylogeny-tree)

## Use

Currently can be used only in typescript NEXT.js projects with styled-components enabled.

To enable typescript follow the [next.js tutorial](https://nextjs.org/docs/basic-features/typescript).

To enable styled-components follow [tutorial and example project](https://styled-components.com/docs/advanced#nextjs).

Add phylodash as subtree ([with sourcetree](https://confluence.atlassian.com/sourcetreekb/adding-a-submodule-subtree-with-sourcetree-785332086.html) or [commandline](https://www.atlassian.com/git/tutorials/git-subtree)) (remote) in the your project git repository.

Install phylodash: `yarn add link:./phylodash`

```typescript
import 'mapbox-gl/dist/mapbox-gl.css';
import 'react-day-picker/lib/style.css';
import 'react-phylogeny-tree-gl/styles.css';

import PhyloDash from 'phylodash';

const newick = '(((A:0.2, B:0.3):0.3,(C:0.5, D:0.3):0.2):0.3, E:0.7):1.0;';
const columns = [{ id: 'id' }, { id: 'date' }, { id: 'latitude' }, { id: 'longitude' }];
const metadata = {
  A: { date: { year: 2020, month: 1 }, latitude: -17.8178, longitude: 31.0447 },
  B: { latitude: -17.8178, longitude: 31.0447, date: { year: 2020, month: 4, day: 1 } },
  C: { latitude: 6.4, longitude: 2.52, date: { year: 2020, month: 3, day: 17 } },
  D: { latitude: -15.4166, longitude: 28.2833, date: { year: 2020, month: 3, day: 16 } },
  E: { latitude: 5.55, longitude: -0.2167, date: { year: 2020, month: 3, day: 25 } },
};
const colors = { '2020-1': '#0d0887', '2020-3': '#fada24', '2020-4': '#5302a3' };
const colorKey = 'year-month';
const mapBoxToken = process.env.MAPBOX_TOKEN;

export async function getStaticProps() {
  const mapBoxToken = process.env.MAPBOX_TOKEN;
  return {
    props: {
      mapBoxToken: mapBoxToken,
    },
  };
}

export function YourComponent(props): JSX.Element {
  return (
    <PhyloDash
      metadata={metadata}
      columns={columns}
      colors={colors}
      colorColumn={colorKey}
      newick={newick}
      showHelp={false}
      mapboxApiAccessToken={props.mapBoxToken}
    />
  );
}
```

## Properties

```typescript
type CustomDate = { year: number; month?: number; day?: number };

export type MetadataValue = {
  date: CustomDate;
  latitude: number;
  longitude: number;
  description?: string;
  country?: string;
  region?: string;
  'PANGO-lineage'?: string;
  variants?: string[];
};

export type Metadata<T> = {
  [key: string]: T;
};

export type Colors = { [key: string]: string };

export type PhyloDashPros<T extends MetadataValue> = {
  metadata: Metadata<T>;
  columns: ColumnDef[];
  colors: Colors;
  colorColumn: string;
  newick: string;
  showHelp: boolean;
  mapboxApiAccessToken: string;
  onDownloadClick?: (ids: iSet<string>) => void;
  key?: unknown;
};
```

- `metadata`: object containing metadata where keys are accession ids and values are MetadataValue which must contain CustomDate object under `date` key, `latitude`, `longitude`
- `columns`: array of objects defining columns for table. Compatible with react-table. Require to have a `id` key
- `colors`: object with keys which are unique values in current color column in metadata and string of RGB code, e.g.: {Benin: "#0d0887"}
- `colorColumn`: string used to match value from objects in `metadata` with key in `colors`. It can be `year-month` or any `id` defined in `columns`
- `newick` newick string to build tree
- `showHelp` boolean indicated if help should be shown
- `mapboxApiAccessToken`
- `onDownloadClick`
- `key` in case that newick or metadata change it is necessary to reinitialise state of the component (useReducer). To force react to do state reinitialisation pass new key.

## Interactions

### Tree

- click on node - highlight node
- click on node with cmd/ctrl - add/remove node to selection
- click on branch - highlight nodes
- click space - deselect
- view subtree
- redraw original tree

### Map

- click on marker - highlight marker
- click on marker with cmd/ctrl - add/remove node to selection
- click space - deselect

### Table

- Select row
- Deselect row
- filter

## SSR issue

if you run into folowing issue it means that you are importing from file which imports PhylogenyTree which does not support SSR 

```
import { PhylogenyTree } from './components/treeWithMenu';
^^^^^^

SyntaxError: Cannot use import statement outside a module
```
