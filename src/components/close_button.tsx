import styled from 'styled-components';

// The close button: absolute positioning on top left of the
// browser window, white background square with a gray X.
export const CloseButton = styled.button`
  position: relative;
  float: right;
  background: #fff;
  width: 2.5rem;
  height: 2.5rem;
  padding: 0;
  border: 0;
  border-radius: 0.25em;
  cursor: pointer;
  outline: 0;
  box-shadow: 0 0 0.625rem rgba(0, 0, 0, 0.4);

  &:before,
  &:after {
    content: '';
    position: absolute;
    top: 1.2rem;
    left: 0.25rem;
    width: 2rem;
    height: 0.1rem;
    background-color: #888;
  }

  &:before {
    transform: rotate(45deg);
  }
  &:after {
    transform: rotate(-45deg);
  }

  &:hover:before,
  &:hover:after {
    background-color: '#444';
  }
`;
