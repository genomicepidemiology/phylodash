import styled from 'styled-components';

export type TooltipProps = {
  visible: boolean;
};

export const BottomArrowTooltip = styled.div<TooltipProps>`
  visibility: ${(props) => (props.visible ? 'visible' : 'hidden')};
  position: absolute;
  background: #c9dfeb;
  line-height: 1.1;
  z-index: 1;
  width: 80%;
  bottom: calc(2vw + 10px);
  left: 50%;
  transform: translateX(-50%);
  padding: 10px;
  border-radius: 6px;

  :after {
    content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    border: solid transparent;
    border-width: 10px;
    margin-left: -10px;
    border-color: #c9dfeb transparent transparent transparent;
    pointer-events: none;
  }
`;

export const RoundTooltip = styled.div<TooltipProps>`
  visibility: ${(props) => (props.visible ? 'visible' : 'hidden')};
  position: absolute;
  background: #c9dfeb;
  text-align: left;
  z-index: 1;
  width: 95%;
  left: 50%;
  top: 10%;
  transform: translateX(-50%);
  padding: 10px;
  border-radius: 6px;
`;
