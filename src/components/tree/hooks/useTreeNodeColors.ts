import { Map as iMap } from 'immutable';
import React from 'react';

import { Colors, Metadata, MetadataValue, TreeNodeColors } from '../../../types';
import { getValueForUniqueColor } from '../../../utils';

export function useTreeNodeColors<T extends MetadataValue>(
  metadata: Metadata<T>,
  leafToAccession: iMap<string, string[]>,
  colors: Colors,
  colorColumn
) {
  return React.useMemo(
    () => calcTreeNodeColors(metadata, leafToAccession, colors, colorColumn),
    [colorColumn, colors, leafToAccession, metadata]
  );
}

export function calcTreeNodeColors<T extends MetadataValue>(
  metadata: Metadata<T>,
  leafToAccession: iMap<string, string[]>,
  colors: Colors,
  colorKey: string
): TreeNodeColors {
  const tree_styles: TreeNodeColors = {};
  for (const [label, accessions] of leafToAccession.entries()) {
    const firstValue = getValueForUniqueColor(colorKey, metadata[accessions[0]]);
    if (!firstValue) continue;
    const allSame = accessions.every((id) => {
      const value = getValueForUniqueColor(colorKey, metadata[id]);
      return value === firstValue;
    });
    if (allSame) {
      const color = colors[firstValue] || null;
      tree_styles[label] = { fillColour: color };
    }
  }
  return tree_styles;
}
