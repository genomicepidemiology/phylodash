import { Map as iMap } from 'immutable';
import React from 'react';

import { getLeafLabels } from '../utils/newick_util';

export type TreeLabelMappers = {
  leafToAccession: iMap<string, string[]>;
  accessionToLeaf: iMap<string, string>;
};

export function useTreeMappers(newick: string): TreeLabelMappers {
  return React.useMemo(() => {
    const leafLabels = getLeafLabels(newick);
    const treeMappers = createLeafSampleMapper(leafLabels);
    return treeMappers;
  }, [newick]);
}

// call upon creation of tree, set WorkingIDs
export function createLeafSampleMapper(leafLabels: string[]) {
  const accessionToLeaf = iMap<string, string>().asMutable();
  const leafToAccession = iMap<string, string[]>().asMutable();
  leafLabels.forEach((label) => {
    const accessions = label.split(' ');
    leafToAccession.set(label, accessions);
    accessions.forEach((id) => {
      accessionToLeaf.set(id, label);
    });
  });

  return { leafToAccession, accessionToLeaf };
}
