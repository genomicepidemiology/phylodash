import { Newick } from 'react-phylogeny-tree-gl/types';

import { Metadata, MetadataValue, Colors } from '../../../types';
import { useTreeMappers } from './useTreeMappers';
import { useTreeNodeColors } from './useTreeNodeColors';

export function useTree<T extends MetadataValue>(
  newick: Newick,
  metadata: Metadata<T>,
  colors: Colors,
  colorColumn: string
) {
  const treeLabelMappers = useTreeMappers(newick);
  const treeNodeColors = useTreeNodeColors(
    metadata,
    treeLabelMappers.leafToAccession,
    colors,
    colorColumn
  );

  return { treeLabelMappers, treeNodeColors };
}
