import { Map as iMap, Set as iSet } from 'immutable';

export function getHighlightedLables(
  accession: iSet<string>,
  accessionToLeaf: iMap<string, string>
): string[] {
  const highlightedLables = [];
  accession.forEach((id) => {
    const label = accessionToLeaf.get(id);
    highlightedLables.push(label);
  });
  return highlightedLables;
}

export function getHighlightedIDsFromLabels(
  lables: string[],
  leafToAccession: iMap<string, string[]>
): iSet<string> {
  const accessions = iSet<string>().withMutations((mutable) => {
    lables.forEach((label) => {
      const sampleIDs = leafToAccession.get(label);
      sampleIDs.forEach((id) => {
        mutable.add(id);
      });
    });
  });
  return accessions;
}
