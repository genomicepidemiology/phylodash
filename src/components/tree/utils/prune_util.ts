import { parse_newick, parse_json } from 'biojs-io-newick';

type TreeNode = {
  name: string;
  children: Children;
  branch_length?: number;
};
type TreeLeaf = Omit<TreeNode, 'children'>;
type Children = (TreeNode | TreeLeaf)[];

export function pruneNewick(newick: string, keepLeafs: Set<string>): string {
  const rootNode = parse_newick(newick);
  const { node: prunedRootNode } = pruneTree(rootNode, keepLeafs);
  const nonTanglingRoot = findNontanglingRoot(prunedRootNode);
  return parse_json(nonTanglingRoot);
}

function pruneTree(node: TreeNode | TreeLeaf, keepIDs: Set<string>) {
  if ('children' in node && Array.isArray(node.children)) {
    const keepChildren = node.children.reduce((acc, current) => {
      const { keep, node: child } = pruneTree(current, keepIDs);
      if (keep) acc.push(child);
      return acc;
    }, [] as Children);
    if (keepChildren.length) {
      // have children which are in keepIDs
      node.children = keepChildren;
      return { keep: true, node };
    }
    // does not have children which are in keepIDs
    return { keep: false };
  }
  // is a leaf
  const accessions = node.name.split(' '); // because of space separated accessions in multilabel leafs
  if (accessions.some((id) => keepIDs.has(id))) {
    return { keep: true, node };
  }
  return { keep: false };
}

function findNontanglingRoot(root: TreeNode | TreeLeaf) {
  let current = root;
  while (
    'children' in current &&
    Array.isArray(current.children) &&
    current.children.length === 1
  ) {
    current = current.children[0];
  }
  return current;
}
