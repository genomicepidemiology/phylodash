import { parse_newick } from 'biojs-io-newick';

function getPostOrderTraversal(treeObj: Tree): Tree[] {
  const nodes = [];
  const queue = [treeObj];

  while (queue.length) {
    const node = queue.pop();
    if (Array.isArray(node.children)) {
      queue.push(...node.children);
    }
    nodes.push(node);
  }

  return nodes.reverse();
}

const isLeaf = (node) => !Array.isArray(node.children);

type Tree = {
  children: Tree[];
  name: string | number;
  branch_length: number;
};

function getTreeLeaves(treeObj: Tree) {
  const postorderTraversal = getPostOrderTraversal(treeObj);

  const leafsLabels = [];
  // bottom-up traversal starting from leaves to root
  for (let nodeIndex = 0; nodeIndex < postorderTraversal.length; nodeIndex++) {
    const node = postorderTraversal[nodeIndex];
    if (isLeaf(node)) leafsLabels.push(node.name);
  }
  return leafsLabels;
}

export function getLeafLabels(newick: string): string[] {
  const tree = parse_newick(newick);
  const labels = getTreeLeaves(tree);
  return labels;
}
