import React from 'react';
import styled from 'styled-components';

import { CloseButton } from '../../close_button';

export function SubtreeHelpOverlay({ leaf }: { leaf?: string }): JSX.Element {
  const [visible, setVisible] = React.useState(Boolean(leaf));

  return (
    <RoundTooltip visible={visible}>
      <CloseButton
        onClick={() => {
          setVisible(false);
        }}
      />
      You are seeing a sub-tree around {leaf} sample, right click anywhere around the tree to open
      contex menu to re-draw full tree.
      <br />
    </RoundTooltip>
  );
}

export const RoundTooltip = styled.div<{ visible: boolean }>`
  visibility: ${(props) => (props.visible ? 'visible' : 'hidden')};
  position: absolute;
  background: #c9dfeb;
  text-align: left;
  z-index: 1;
  width: 95%;
  @media (min-width: 576px) {
    width: 32rem;
  }
  min-height: 5rem;
  left: 50%;
  top: 10%;
  transform: translateX(-50%);
  margin: 0.8rem;
  padding: 1rem;
  border-radius: 6px;
`;
