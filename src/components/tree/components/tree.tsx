// import { plugins as phylocanvasPlugins } from '@phylocanvas/phylocanvas.gl';
import { Set as iSet } from 'immutable';
import React from 'react';
import PhylogenyTree from 'react-phylogeny-tree-gl';
import { useLeafSubtree, useAutoResize } from 'react-phylogeny-tree-gl/hooks';
import {
  createOnSelectPlugin,
  createOnViewSubtreePlugin,
  createOnRedrawReRootTreePlugin,
  createRedoUndoPlugin,
  onClickHighlightOffsprings,
  scalebar,
} from 'react-phylogeny-tree-gl/plugins';
import styled from 'styled-components';

import { TreeNodeColors, TreeRef } from '../../../types';
import { TreeHelp } from '../../help/helpTooltips';
import { ShowHelpContext } from '../../main';
import { TreeLabelMappers } from '../hooks/useTreeMappers';
import { getHighlightedIDsFromLabels, getHighlightedLables } from '../utils/id_mappers';
import { SubtreeHelpOverlay } from './subtree_help_overlay';

const hooks = [useAutoResize, useLeafSubtree];

type TreeActions = {
  setSelectedIDs: (ids: iSet<string>) => void;
  deselectIDs: () => void;
  setWorkingIDs: (workingIDs: iSet<string>, rootId?: string, keepLeafSubtreeID?: boolean) => void;
};

export type TreeProps = {
  newick: string;
  treeLabelMappers: TreeLabelMappers;
  leafColors: TreeNodeColors;
  selectedIDs: iSet<string>;
  subtreeBaseLeaf: string | undefined;
  rootId?: string;
  noLevelsUp: number;
  minBranchLength: number;
  actions: TreeActions;
};



export const Tree = React.forwardRef(TreeComponent);
function TreeComponent(
  {
    newick,
    treeLabelMappers,
    rootId,
    leafColors,
    selectedIDs,
    subtreeBaseLeaf,
    noLevelsUp,
    minBranchLength,
    actions,
  }: TreeProps,
  ref?: React.RefObject<TreeRef>
): JSX.Element {
  const showHelp = React.useContext(ShowHelpContext);

  const selectedLeafs = React.useMemo(() => {
    return getHighlightedLables(selectedIDs, treeLabelMappers.accessionToLeaf);
  }, [selectedIDs, treeLabelMappers.accessionToLeaf]);

  const initOptions = React.useMemo(
    () => ({
      source: newick,
      rootId: null,
      interactive: true,
      nodeSize: 7,
      haloRadius: 6,
      haloWidth: 2,
      scalebar: true,
      highlightColour: tuple(52, 182, 199, 255),
    }),
    [newick]
  );
  const controlledOptions = React.useMemo(
    () => ({
      selectedIds: selectedLeafs,
      styles: leafColors,
      leafSubtree: {
        leafID: subtreeBaseLeaf,
        noLevels: noLevelsUp,
        minLeafToRootLength: minBranchLength,
      },
    }),
    [leafColors, minBranchLength, noLevelsUp, selectedLeafs, subtreeBaseLeaf]
  );
  const plugins = React.useMemo(() => {
    return [
      createRedoUndoPlugin(),
      createOnSelectPlugin((_tree, selectedLeafLabels) => {
        if (selectedLeafLabels && selectedLeafLabels.length) {
          const selectedIds = getHighlightedIDsFromLabels(
            selectedLeafLabels,
            treeLabelMappers.leafToAccession
          );
          actions.setSelectedIDs(selectedIds);
        } else {
          actions.deselectIDs();
        }
      }),
      createOnViewSubtreePlugin((tree, leafsInTree) => {
        const IDsInTree = getHighlightedIDsFromLabels(
          leafsInTree,
          treeLabelMappers.leafToAccession
        );
        actions.setWorkingIDs(IDsInTree, tree.props.rootId, true);
      }),
      createOnRedrawReRootTreePlugin((tree, leafsInTree) => {
        const IDsInTree = getHighlightedIDsFromLabels(
          leafsInTree,
          treeLabelMappers.leafToAccession
        );
        actions.setWorkingIDs(IDsInTree, tree.props.rootId, false);
      }),
      onClickHighlightOffsprings,
      scalebar,
    ];
  }, [actions, treeLabelMappers.leafToAccession]);

  React.useEffect(() => {
    if (ref.current) {
      const tree = ref.current.getTree();
      if (tree.props.rootId !== rootId) {
        tree.setRoot(rootId);
      }
    }
  }, [rootId, ref]);

  return (
    <TreeContainer>
      <PhylogenyTree
        ref={ref}
        initProps={initOptions}
        controlledProps={controlledOptions}
        hooks={hooks}
        plugins={plugins}
      />

      <TreeHelp visible={showHelp}></TreeHelp>
      <SubtreeHelpOverlay leaf={subtreeBaseLeaf} />
    </TreeContainer>
  );
}

const TreeContainer = styled.div`
  grid-area: phylocanvas;
  border: #000 1px;
  position: relative;
`;

const tuple = <T extends unknown[]>(...args: T): T => args;
