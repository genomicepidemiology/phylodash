import React from 'react';
import { Marker } from 'react-map-gl';
import styled, { css } from 'styled-components';

import { ColorCounts, GeoPosition } from './types';

type MarkerWrapperProps = {
  location: GeoPosition;
  highlighted: boolean;
  colorCounts: ColorCounts;
  setSelectedLoc: (location: GeoPosition, metaOrCtrl: boolean) => void;
};

export function MarkerWrapper({
  location,
  highlighted,
  colorCounts,
  setSelectedLoc,
}: MarkerWrapperProps): JSX.Element {
  const [lat, lgn] = location;
  const onClick = React.useCallback(
    (event: React.MouseEvent<SVGSVGElement>) => {
      const metaOrCtrl = event.metaKey || event.ctrlKey;
      setSelectedLoc(location, metaOrCtrl);
    },
    [location, setSelectedLoc]
  );

  return (
    <Marker latitude={lat} longitude={lgn} offsetTop={-11} offsetLeft={-11}>
      <DivContainer highlighted={highlighted}>
        <MemoPieMarker colorCounts={colorCounts} onClick={onClick} />
      </DivContainer>
    </Marker>
  );
}

const DivContainer = styled('div')<{ highlighted?: boolean }>`
  height: 20px;
  width: 20px;
  padding: 2px 4px;
  ${(props) =>
    props.highlighted &&
    css`
      border: 2px solid #34b6c7;
      border-radius: 50%;
      padding: 0px 2px;
    `}
`;

const MemoPieMarker = React.memo(PieMarker);

function PieMarker({
  colorCounts,
  onClick,
}: {
  colorCounts: ColorCounts;
  onClick: React.MouseEventHandler<SVGSVGElement>;
}): JSX.Element {
  return (
    <svg
      onClick={onClick}
      viewBox="-1 -1 2 2"
      style={{ transform: 'rotate(90deg)', height: 12, cursor: 'pointer' }}
    >
      {getSlices(colorCounts)}
    </svg>
  );
}

function getSlices(colorCounts: ColorCounts) {
  const slices = [];
  let cumulativePercent = 0;
  let total = 0;
  for (const count of colorCounts.values()) {
    total += count;
  }
  for (const [color, count] of colorCounts.entries()) {
    const fraction = count / total;
    // destructuring assignment sets the two variables at once
    const [startX, startY] = getCoordinatesForPercent(cumulativePercent);

    // each slice starts where the last slice ended, so keep a cumulative percent
    cumulativePercent += fraction;

    const [endX, endY] = getCoordinatesForPercent(cumulativePercent);

    // if the slice is more than 50%, take the large arc (the long way around)
    const largeArcFlag = fraction > 0.5 ? 1 : 0;

    // create an array and join it just for code readability
    const pathData = [
      `M ${startX} ${startY}`, // Move
      `A 1 1 0 ${largeArcFlag} 1 ${endX} ${endY}`, // Arc
      'L 0 0', // Line
    ].join(' ');

    slices.push(<path key={color} d={pathData} fill={color} />);
  }

  return slices;
}

function getCoordinatesForPercent(fraction: number): [number, number] {
  const x = Math.cos(2 * Math.PI * fraction);
  const y = Math.sin(2 * Math.PI * fraction);
  return [x, y];
}
