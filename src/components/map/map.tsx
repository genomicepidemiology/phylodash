import React from 'react';
import ReactMapGL, { NavigationControl } from 'react-map-gl';
import styled from 'styled-components';

import { MapHelp } from '../help/helpTooltips';

export default function GeoMap(props: {
  mapboxApiAccessToken: string;
  showHelp: boolean;
  deselect: () => void;
  children: JSX.Element;
}): JSX.Element {
  // console.log('maps', props.markerData);
  
  const mapbox_map = React.useRef();

  const [viewport, setViewport] = React.useState({
    latitude: 11.1784,
    longitude: 20.4411,
    zoom: 0,
  });

  function changeViewPort(viewport) {
    setViewport(viewport);
  }

  const navControlStyle = {
    left: 10,
    top: 10,
  };

  return (
    <MapContainer ref={mapbox_map}>
      <ReactMapGL
        {...viewport}
        width="100%"
        height="100%"
        mapStyle="mapbox://styles/mapbox/light-v10"
        onViewportChange={(viewport) => changeViewPort(viewport)}
        mapboxApiAccessToken={props.mapboxApiAccessToken}
        // TODO: transfor to useCalback
        onClick={() => {
          props.deselect();
        }}
      >
        <NavigationControl style={navControlStyle} />
        {props.children}
      </ReactMapGL>
      <MapHelp visible={props.showHelp} />
    </MapContainer>
  );
}

const MapContainer = styled.div`
  grid-area: map;
  position: relative;
`;
