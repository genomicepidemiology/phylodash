import { Set as iSet } from 'immutable';
import React from 'react';

import { Actions } from '../../state/actions';
import { Colors, Metadata, MetadataValue } from '../../types';
import calcMarkerDataIter from './marker_data_trans';

export function useMarkers<T extends MetadataValue>(
  metadata: Metadata<T>,
  colors: Colors,
  colorColumn: string,
  workingIDs: iSet<string>,
  dispatch: React.Dispatch<Actions>
) {
  const [markersData, markersLocToIDs] = React.useMemo(
    () => calcMarkerDataIter(subsetIterator(metadata, workingIDs), colors, colorColumn),
    [metadata, workingIDs, colors, colorColumn]
  );

  const setSelectedLoc = React.useCallback(
    (location, metaOrCtrl) => {
      if (metaOrCtrl)
        dispatch({ type: 'MAP_ADD_SELECT_IDS', location, locationToIDs: markersLocToIDs });
      else dispatch({ type: 'MAP_SELECT_IDS', location, locationToIDs: markersLocToIDs });
    },
    [dispatch, markersLocToIDs]
  );

  return {
    markersData,
    setSelectedLoc,
  };
}

export function* subsetIterator(
  object: { [key: string]: MetadataValue },
  keys?: iSet<string>
): Generator<[string, MetadataValue], void, unknown> {
  if (keys && keys.size > 0) {
    for (const key of keys) if (object[key]) yield [key, object[key]];
  } else {
    for (const [key, value] of Object.entries(object)) {
      yield [key, value];
    }
  }
}
