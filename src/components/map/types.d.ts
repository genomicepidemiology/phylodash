import { Map as iMap, List as iList, Set as iSet } from 'immutable';

export type ColorCounts = iMap<string, number>;
export type GeoPosition = iList<number>;

export type HighlightedLoc = iSet<GeoPosition>;
export type MarkersData = iMap<GeoPosition, ColorCounts>;

export type MarkersLocToIDs = iMap<GeoPosition, iSet<string>>;
