import React from 'react';

import { MarkerWrapper } from './pie_marker';
import { GeoPosition, HighlightedLoc, MarkersData } from './types';

const MemoMarkerWrapper = React.memo(MarkerWrapper);

type MarkersProps = {
  markerData: MarkersData;
  highlightedLoc: HighlightedLoc;
  setSelectedLoc: (location: GeoPosition, metaOrCtrl: boolean) => void;
};

export function Markers({ markerData, highlightedLoc, setSelectedLoc }: MarkersProps): JSX.Element {
  const isHighlighted = isHighlightedFactory(highlightedLoc);

  return (
    <>
      {Array.from(markerData, ([location, colorCounts]) => (
        <MemoMarkerWrapper
          key={location.toString()}
          location={location}
          highlighted={isHighlighted(location)}
          colorCounts={colorCounts}
          setSelectedLoc={setSelectedLoc}
        />
      ))}
    </>
  );
}

function isHighlightedFactory(
  highlightedLoc: HighlightedLoc
): ([lat, lgn]: GeoPosition) => boolean {
  return (location) => highlightedLoc.has(location);
}
