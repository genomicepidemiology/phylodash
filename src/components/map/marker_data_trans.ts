import { List as iList, Map as iMap, Set as iSet } from 'immutable';

import { Colors, MetadataValue } from '../../types';
import { getValueForUniqueColor } from '../../utils';
import { MarkersData, GeoPosition, ColorCounts, MarkersLocToIDs } from './types';

function isLocationDefined(data): boolean {
  return Boolean(
    data && data.latitude && data.longitude && data.latitude !== 'NA' && data.longitude !== 'NA'
  );
}

export default function calcMarkerDataIter(
  metadata: Generator<[string, MetadataValue], void, unknown>, // [string, { latitude: number; longitude: number; [index: string]: number | string }]
  colors: Colors,
  colorKey: string
): [MarkersData, MarkersLocToIDs] {
  const mutableLocToIDs = iMap<GeoPosition, iSet<string>>().asMutable();

  const markersData = iMap<GeoPosition, ColorCounts>().withMutations(
    (mainMap: MarkersData): void => {
      for (const [id, data] of metadata) {
        if (isLocationDefined(data)) {
          const location: GeoPosition = iList([data.latitude, data.longitude]);
          const ids = mutableLocToIDs.get(location, iSet<string>()).add(id);
          mutableLocToIDs.set(location, ids);

          const colorKeyValue = getValueForUniqueColor(colorKey, data);
          const color = colorKeyValue ? colors[colorKeyValue] : '#000000';
          let colorCounts = mainMap.get(location) || iMap([[color, 0]]);
          const count = colorCounts.get(color, 0) + 1;
          if (color) {
            colorCounts = colorCounts.set(color, count);
            mainMap.set(location, colorCounts);
          }
        }
      }
    }
  );
  const markersLocToIDs = mutableLocToIDs.asImmutable();
  return [markersData, markersLocToIDs];
}
