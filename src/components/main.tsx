import React from 'react';
import styled from 'styled-components';

import { Actions, useActionCallbacks } from '../state/actions';
import { initState, State } from '../state/init';
import createReducer from '../state/reducer';
import { MetadataValue, PhyloDashPros, PhyloDashRefProps, TreeRef } from '../types';
import GeoMap from './map/map';
import { Markers } from './map/markers';
import { useMarkers } from './map/useMarkers';
import Table from './table/table';
import { createTableColumnsDef } from './table/utils/columns';
import { arrayTableData } from './table/utils/transform_data';
import { useTimeline } from './timeline/hooks/useTimeline';
import VXTimeline from './timeline/index';
import { Tree } from './tree';
import { useTree } from './tree/hooks/useTree';

export const ShowHelpContext = React.createContext(false);

export default function PhyloDash<T extends MetadataValue>({
  metadata,
  columnsDef,
  colors,
  colorColumn,
  newick,
  showHelp,
  mapboxApiAccessToken,
  onDownloadClick,
  forwardedRef,
}: PhyloDashPros<T>): JSX.Element {
  const { treeLabelMappers, treeNodeColors } = useTree(newick, metadata, colors, colorColumn);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoizedReducer = React.useCallback(
    createReducer({ metadata: metadata }, treeLabelMappers),
    [metadata, treeLabelMappers]
  );
  const [state, dispatch] = React.useReducer<(state: State, action: Actions) => State, null>(
    memoizedReducer,
    null,
    (): State => initState(metadata)
  );

  const actions = useActionCallbacks(dispatch, onDownloadClick);

  const [tableData, selectOptions] = React.useMemo(
    () => arrayTableData(metadata, state.shownIDs),
    [metadata, state.shownIDs]
  );
  const tableColumns = React.useMemo(
    () => createTableColumnsDef(columnsDef, actions.table.controls.onIDChange),
    [columnsDef, actions]
  );

  const timelineData = useTimeline(metadata);

  const { markersData, setSelectedLoc } = useMarkers(
    metadata,
    colors,
    colorColumn,
    state.shownIDs,
    dispatch
  );

  const phylocanvasRef = React.useRef<TreeRef>();

  React.useImperativeHandle(
    forwardedRef,
    (): PhyloDashRefProps => ({
      getTree: () => phylocanvasRef?.current?.getTree(),
      setSelectedIDs: (ids) => {
        if (ids) dispatch({ type: 'SET_IDS_FROM_URL', value: ids });
      },
      setSubtreeBaseLeaf: (leafID, noLevelsUp, minBranchLength) => {
        dispatch({ type: 'SET_SUBTREE_BASE_LEAF', leafID, noLevelsUp, minBranchLength });
      },
    })
  );

  return (
    <Grid>
      <ShowHelpContext.Provider value={showHelp}>
        <GeoMap
          deselect={actions.map.deselect}
          mapboxApiAccessToken={mapboxApiAccessToken}
          showHelp={showHelp}
        >
          <Markers
            markerData={markersData}
            highlightedLoc={state.markerHighlightedLoc}
            setSelectedLoc={setSelectedLoc}
          />
        </GeoMap>

        <TimelineContainer>
          <VXTimeline data={timelineData} />
        </TimelineContainer>

        <Table
          data={tableData}
          columnsDef={tableColumns}
          idFilterVal={state.idFilterValue}
          selectOptions={selectOptions}
          selectedIDs={state.selectedIDs}
          actions={actions.table}
          showHelp={showHelp}
          subtreeBaseLeaf={state.subtreeBaseLeafLabel}
        />

        <Tree
          ref={phylocanvasRef}
          newick={newick}
          treeLabelMappers={treeLabelMappers}
          leafColors={treeNodeColors}
          rootId={state.rootId}
          actions={actions.tree}
          selectedIDs={state.selectedIDs}
          subtreeBaseLeaf={state.subtreeBaseLeafLabel}
          noLevelsUp={state.noLevelsUp}
          minBranchLength={state.minBranchLength}
        />
      </ShowHelpContext.Provider>
    </Grid>
  );
}

const Grid = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr 2.5vw 1fr;
  grid-template-areas:
    'map      phylocanvas'
    'timeline phylocanvas'
    'metadata phylocanvas';
`;

const TimelineContainer = styled.div`
  grid-area: timeline;
  background: #f3f3f3;
  line-height: 0.8;
`;
