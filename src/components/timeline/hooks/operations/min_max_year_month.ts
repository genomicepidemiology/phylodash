import { Month, Operation, Year } from '../useTimeline';

export type YearMonth = { year: Year; month?: Month };

function isYearMonthLater(current: YearMonth, prev: YearMonth) {
  // prettier-ignore
  return current.year > prev.year ? true
    : current.year < prev.year ? false
    // current.year == prev.year
    : !current.month ? false 
    : !prev.month ? true
    :  current.month > prev.month;
}

export const getOlder: Operation<YearMonth> = (_id, record, accumulator) => {
  const { year, month } = record.date;
  const currentYearMonth = month ? { year, month } : { year };
  return accumulator && isYearMonthLater(currentYearMonth, accumulator)
    ? accumulator
    : currentYearMonth;
};

export const getNewer: Operation<YearMonth> = (_id, record, accumulator) => {
  const { year, month } = record.date;
  const currentYearMonth = month ? { year, month } : { year };
  // prettier-ignore
  return !accumulator ? currentYearMonth 
    : isYearMonthLater(currentYearMonth, accumulator) ? currentYearMonth 
    : accumulator;
};
