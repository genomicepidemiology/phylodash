import { Map as iMap } from 'immutable';

import { Month, Operation, Year } from '../useTimeline';

export type MonthCount = number;
export type YearCount = { total: number; months: iMap<Month, MonthCount> };

export const addToYearMonthMap: Operation<iMap<Year, YearCount>> = (_id, record, accumulator) => {
  const { year, month } = record.date;

  if (month && accumulator) {
    const defaultYearRec: YearCount = { total: 0, months: iMap([[month, 0]]) };
    const { total, months } = accumulator.get(year, defaultYearRec);
    const monthCount = months.get(month, 0);
    const newYearRec = { total: total + 1, months: months.set(month, monthCount + 1) };
    const updatedAcc = accumulator.set(year, newYearRec);
    return updatedAcc;
  }
  if (accumulator) {
    const defaultYearRec = { total: 0, months: iMap<Month, MonthCount>() };
    const { total, months } = accumulator.get(year, defaultYearRec);
    const newYearRec = { total: total + 1, months };
    const updatedAcc = accumulator.set(year, newYearRec);
    return updatedAcc;
  }
  return iMap([[year, { total: 1, months: iMap<Month, MonthCount>() }]]);
};
