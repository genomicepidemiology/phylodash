import { Map as iMap } from 'immutable';

import { YEAR_BIN_THRESHOLD } from '..';
import { YearCount } from './operations/add_year_month';
import { YearMonth } from './operations/min_max_year_month';
import { Year } from './useTimeline';

export function assembleTimelineData(
  yearMonthCount: iMap<Year, YearCount>,
  minDate: YearMonth,
  maxDate: YearMonth
): { date: YearMonth; count: number; yearTotal?: number }[] {
  const noYears = maxDate.year - minDate.year + 1;
  if (noYears < YEAR_BIN_THRESHOLD) {
    return getMonthBins(yearMonthCount, minDate, maxDate);
  }
  return getYearBins(yearMonthCount, minDate.year, noYears);
}

function getMonthBins(
  yearMonthCount: iMap<Year, YearCount>,
  minDate: YearMonth,
  maxDate: YearMonth
) {
  const noMonths = getNoMonths(minDate, maxDate);
  const startMonth = minDate.month ? minDate.month - 1 : 0;

  const result = Array.from({ length: noMonths }, (_, index) => {
    const binDate = new Date(minDate.year, startMonth + index);
    const year = binDate.getFullYear();
    const month = binDate.getMonth() + 1; // Date have zero index months
    const { total, months } = yearMonthCount.get(year, { total: 0, months: null });
    const count = months ? months.get(month, 0) : 0;
    return { date: { year, month }, count, yearTotal: total };
  });
  return result;
}

function getYearBins(yearMap: iMap<Year, YearCount>, startYear: Year, noYears: number) {
  const result = Array.from({ length: noYears }, (_, index) => {
    const year = startYear + index;
    const { total } = yearMap.get(year, { total: 0, months: null });
    return { date: { year }, count: total };
  });
  return result;
}

function getNoMonths(minDate: YearMonth, maxDate: YearMonth) {
  const { year: minYear, month: minMonth = 1 } = minDate;
  const { year: maxYear, month: maxMonth = 1 } = maxDate;
  return 12 * (maxYear - minYear) + (maxMonth - minMonth + 1);
}
