import React from 'react';

import { Metadata, MetadataValue } from '../../../types';
import { addToYearMonthMap } from './operations/add_year_month';
import { getOlder, getNewer } from './operations/min_max_year_month';
import { reduceMetadata } from './reduce_metadata';
import { assembleTimelineData } from './transformation';

export type Year = number;
export type Month = number;

export type Operation<AccumulatorType> = (
  id: string,
  record: MetadataValue,
  accumulator: AccumulatorType
) => AccumulatorType;

export type Operations = {
  [key: string]: Operation<unknown>;
};

const operations = {
  minYearMonth: getOlder,
  maxYearMonth: getNewer,
  yearMonthCount: addToYearMonthMap,
};

export function useTimeline<T extends MetadataValue>(metadata: Metadata<T>) {
  const data = React.useMemo(() => {
    const { minYearMonth, maxYearMonth, yearMonthCount } = reduceMetadata(metadata, operations);
    const timelineData = assembleTimelineData(yearMonthCount, minYearMonth, maxYearMonth);
    return timelineData;
  }, [metadata]);
  return data;
}
