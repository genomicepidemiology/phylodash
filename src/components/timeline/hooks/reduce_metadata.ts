import { Metadata, MetadataValue } from '../../../types';
import { Operation, Operations } from './useTimeline';

type Reduction<O extends Operations> = { [key in keyof O]: ReturnType<O[key]> };

export function reduceMetadata<O extends Operations, T extends MetadataValue>(
  metadata: Metadata<T>,
  operations: O
): Reduction<O> {
  const iterOperations: [name: keyof O, operation: Operation<unknown>][] =
    Object.entries(operations);
  return Object.entries(metadata).reduce((resultAccumulator, [id, record]) => {
    return iterOperations.reduce((acc, [name, operation]) => {
      return {
        ...acc,
        [name]: operation(id, record, acc[name]),
      };
    }, resultAccumulator);
  }, {} as Reduction<O>);
}
