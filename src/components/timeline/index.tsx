import { AxisBottom } from '@visx/axis';
import { localPoint } from '@visx/event';
import { Group } from '@visx/group';
import { HeatmapRect } from '@visx/heatmap';
import { scaleUtc, scaleLinear } from '@visx/scale';
import { useTooltip, useTooltipInPortal } from '@visx/tooltip';
import { scaleSequential } from 'd3-scale';
import { interpolateViridis } from 'd3-scale-chromatic';
import React from 'react';

import { TimelineHelp } from '../help/helpTooltips';
import { ShowHelpContext } from '../main';
import { YearMonth } from './hooks/operations/min_max_year_month';

export type TimelineDatum = { date: YearMonth; count: number; yearTotal?: number };

export type TimelineProps = {
  data: TimelineDatum[];
};

export const YEAR_BIN_THRESHOLD = 30;

const width = 800;
const height = 40;

// accessors
const bins = (d: TimelineDatum) => [d.count];
const count = (d: number) => d;

const yScale = scaleLinear({
  domain: [0, 0],
  range: [0, 0],
});

// const testDataYear = Array.from({ length: 10 }, (_, index) => {
//   const date = new Date(Date.UTC(2000 + index, 0));
//   const year = date.getFullYear();
//   // const month = date.getMonth()+1;
//   return { date: { year }, count: index };
// });

// const testDataMonths = Array.from({ length: 20 }, (_, index) => {
//   const date = new Date(Date.UTC(2000, index));
//   const year = date.getFullYear();
//   const month = date.getMonth()+1;
//   return { date: { year, month }, count: index };
// });

export default function Timeline(props: TimelineProps) {
  const showHelp = React.useContext(ShowHelpContext);

  const { tooltipData, tooltipLeft, tooltipTop, tooltipOpen, showTooltip, hideTooltip } =
    useTooltip();

  const { containerRef, TooltipInPortal } = useTooltipInPortal({
    // use TooltipWithBounds
    detectBounds: true,
    // when tooltip containers are scrolled, this will correctly update the Tooltip position
    scroll: false,
  });

  const data = props.data;
  // const data = testDataYear;
  // const data = testDataMonths;

  const noColumns = data.length;

  const xScale = scaleLinear({
    domain: [0, noColumns],
    range: [0, width],
  });

  // const colorMax = max(data, count);
  const colorMax = max(data, (d) => max(bins(d), count));
  const rectColorScale = scaleSequential<string, never>(interpolateViridis).domain([0, colorMax]);

  const binWidth = width / noColumns;
  const startDate = getStartDate(data[0].date);

  const endDate = getDomainEnd(data);
  // console.log(data[0].date, startDate);
  // console.log(data[data.length - 1].date, endDate);

  const timeScaleX = scaleUtc({
    domain: [startDate, endDate],
    range: [0, width - binWidth],
  });

  // console.log('domain',timeScaleX.domain());
  // console.log('ticks', timeScaleX.ticks(data.length-1));

  return (
    <div style={{ position: 'relative' }}>
      <svg
        ref={containerRef}
        preserveAspectRatio="xMidYMin meet"
        viewBox={`0 0 ${width} ${height}`}
      >
        <Group className="visx-heatmap-rects">
          <HeatmapRect
            data={data}
            bins={bins}
            count={count}
            xScale={xScale}
            yScale={yScale}
            colorScale={rectColorScale}
            binWidth={binWidth}
            binHeight={height * 0.5}
            gap={0}
            x0={0}
          >
            {(heatmap) =>
              heatmap.map((heatmapBins) =>
                heatmapBins.map((bin) => (
                  <rect
                    key={`heatmap-rect-${bin.row}-${bin.column}`}
                    className="visx-heatmap-rect"
                    width={bin.width}
                    height={bin.height}
                    x={bin.x}
                    y={bin.y}
                    fill={bin.color}
                    fillOpacity={bin.opacity}
                    onMouseMove={(event) => {
                      const coords = localPoint(event);
                      // console.log(coords);
                      const {
                        date: { year, month },
                        count,
                        yearTotal,
                      } = bin.datum;
                      showTooltip({
                        tooltipLeft: coords.x,
                        tooltipTop: coords.y,
                        tooltipData: (
                          <>
                            {`no. samples in ${year}`}
                            {month ? `-${month}` : null}
                            {`: ${count}`}
                            {yearTotal ? (
                              <>
                                <br />
                                {`samples in whole year: ${yearTotal}`}
                              </>
                            ) : null}
                          </>
                        ),
                      });
                    }}
                    onMouseLeave={hideTooltip}
                  />
                ))
              )
            }
          </HeatmapRect>
        </Group>
        <AxisBottom scale={timeScaleX} top={height * 0.5} tickLength={4} left={binWidth} />
      </svg>
      {tooltipOpen && (
        <TooltipInPortal
          // set this to random so it correctly updates with parent bounds
          key={Math.random()}
          top={tooltipTop}
          left={tooltipLeft}
        >
          <strong>{tooltipData}</strong>
        </TooltipInPortal>
      )}
      <TimelineHelp visible={showHelp} />
    </div>
  );
}

function max(data, value) {
  return Math.max(...data.map(value));
}

function getStartDate({ year, month = null }: YearMonth) {
  const offset = 1;
  if (month) {
    return new Date(Date.UTC(year, month - 1 + offset));
  }
  const startDate = new Date(Date.UTC(year + offset, 0));
  return startDate;
}

function getDomainEnd(data: TimelineDatum[]) {
  const { year, month } = data[data.length - 1].date;
  return month ? new Date(Date.UTC(year, month, 1)) : new Date(Date.UTC(year, 12, 1, 0, 0, 0, -1));
}
