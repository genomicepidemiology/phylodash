import styled from 'styled-components';

export const Button = styled.button`
  margin: 3px;
  padding: 0.25em 3px;
  background-color: #c9dfeb;
  border: 3px solid #c9dfeb;
  border-radius: 4px;

  :hover {
    border-color: #4670b1;
  }
`;
