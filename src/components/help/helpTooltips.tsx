import Image from 'next/image';
import React from 'react';
import styled from 'styled-components';

import { RoundTooltip, BottomArrowTooltip, TooltipProps } from '../tooltips';

const HelpTitle = styled.h3`
  margin-top: 0;
  margin-bottom: 0.5rem;
`;

export function TimelineHelp({ visible }: TooltipProps) {
  return (
    <BottomArrowTooltip visible={visible}>
      <HelpTitle>Timeline with heatmap</HelpTitle>
      <ul>
        <li>Heatmap shows number of samples in the time period</li>
      </ul>
    </BottomArrowTooltip>
  );
}

export function ColorColumnHelp({ visible }: TooltipProps) {
  return (
    <BottomArrowTooltip visible={visible}>
      <p>Select different metadata column for samples color</p>
    </BottomArrowTooltip>
  );
}

export function MapHelp({ visible }: TooltipProps) {
  return (
    <RoundTooltip visible={visible}>
      <HelpTitle>Map</HelpTitle>
      <ul>
        <li>Marker located according to country</li>
        <li>
          Colours in the piechart marker are shown according to the distribution of unique values in
          the selected colour column
        </li>
        <li>Default color category is month</li>
      </ul>
    </RoundTooltip>
  );
}

export function TableHelp({ visible }: TooltipProps) {
  return (
    <RoundTooltip visible={visible}>
      <HelpTitle>Metadata Table</HelpTitle>
      <ul>
        <li>
          Scroll <span style={{ fontSize: 14 }}>⬍</span> and <span style={{ fontSize: 14 }}>⬌</span>{' '}
        </li>
        <li>
          Use filters to search samples, then click the checkbox in the table header or sub-select
          samples in the table to highlight these sample(s) on tree and map
        </li>
      </ul>
    </RoundTooltip>
  );
}

export function TreeHelp({ visible }: TooltipProps) {
  return (
    <RoundTooltip visible={visible}>
      <TreeHelpGrid>
        <TextArea>
          <HelpTitle>Tree</HelpTitle>
          <ul>
            <li>
              Click on leaf to highlight it on tree and map, and to filter it in metadata table
            </li>
            <li>
              Click on a node to select leaves in the clade and on the map, and to filter them in
              the metadata table
            </li>
            <li>Hover over leaf to show tooltip</li>
            <li>Zoom with scrolling</li>
            <li>Hold CTRL/Control + scroll to increase horizontal distance between nodes</li>
            <li>Hold ALT/Option + scroll to increase vertical distance between nodes</li>
          </ul>
        </TextArea>
        <LeftPicture>
          <p>Right click on tree node:</p>
          <Image
            src="/img/left_contex_menu.png"
            quality={95}
            layout="intrinsic"
            width={256}
            height={424}
          />
        </LeftPicture>
        <RightPicture>
          <p>Right click on background:</p>
          <Image
            src="/img/right_context_menu.png"
            quality={95}
            layout="intrinsic"
            width={256}
            height={424}
          />
        </RightPicture>
      </TreeHelpGrid>
    </RoundTooltip>
  );
}

const TreeHelpGrid = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  padding: 1.2rem;
  /* grid-template-rows: 1fr 1fr; */
  grid-template-areas:
    'textArea textArea'
    'left right';
`;

const TextArea = styled.div`
  grid-area: textArea;
`;

const LeftPicture = styled.div`
  grid-area: left;
`;

const RightPicture = styled.div`
  grid-area: right;
`;
