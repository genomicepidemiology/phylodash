import React from 'react';
import { CellProps, HeaderProps, TableToggleAllRowsSelectedProps } from 'react-table';

import { MetadataValue } from '../../../types';
import { TableData } from '../types';

export function checkBoxUIHook(hooks) {
  hooks.visibleColumns.push((columns) => [
    // Let's make a column for selection
    {
      id: 'selection',
      // The header can use the table's getToggleAllRowsSelectedProps method
      // to render a checkbox
      Header: HeaderSelectBox,
      // The cell can use the individual row's getToggleRowSelectedProps method
      // to the render a checkbox
      // Filter: headerSelectBox,
      Cell: rowSelectBox,
      width: 85,
    },
    ...columns,
  ]);
}

function HeaderSelectBox<T extends MetadataValue>({
  getToggleAllVisibleRowsSelectedProps,
  state,
  rows,
}: HeaderProps<TableData<T>>) {
  const toggleProps = getToggleAllVisibleRowsSelectedProps();
  const isFiltered = state.filters.length > 0;
  return (
    <>
      {SelectDescription(toggleProps.checked, isFiltered, rows.length)}
      <IndeterminateCheckbox {...toggleProps} />
    </>
  );
}

function SelectDescription(checked: boolean, isFiltered: boolean, rowsLength: number): JSX.Element {
  const plural = rowsLength > 1 ? 's' : '';
  if (checked && isFiltered)
    return (
      <div>
        Deselect {rowsLength}
        <br />
        filtered row{plural}
      </div>
    );
  if (!checked && isFiltered)
    return (
      <div>
        Select {rowsLength}
        <br />
        filtered row{plural}
      </div>
    );
  if (checked && !isFiltered)
    return (
      <div>
        Deselect all
        <br />
        {rowsLength} row{plural}
      </div>
    );
  return (
    <div>
      Select all
      <br />
      {rowsLength} row{plural}
    </div>
  );
}

function rowSelectBox<T extends MetadataValue>({ row }: CellProps<TableData<T>>) {
  const { indeterminate, ...rowProps } = row.getToggleRowSelectedProps();
  return <input type="checkbox" {...rowProps} style={{ transform: 'scale(1.5)' }} />;
}

function IndeterminateCheckbox({ indeterminate, ...rest }: TableToggleAllRowsSelectedProps) {
  const defaultRef = React.useRef<HTMLInputElement>();

  React.useEffect(() => {
    defaultRef.current.indeterminate = indeterminate;
  }, [defaultRef, indeterminate]);

  return <input type="checkbox" ref={defaultRef} {...rest} style={{ transform: 'scale(1.5)' }} />;
}
