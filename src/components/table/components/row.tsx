import { Properties } from 'csstype';
import React from 'react';
import { Cell, Row } from 'react-table';
import styled from 'styled-components';

import { MetadataValue } from '../../../types';
import { TableData } from '../types';

type RowRenderProps<T extends MetadataValue> = {
  data: {
    rows: Row<TableData<T>>[];
    prepareRow: (row: Row<TableData<T>>) => void;
  };
  index: number;
  style: Properties<string | number>;
};

export function RowRender<T extends MetadataValue>({
  data,
  index,
  style,
}: RowRenderProps<T>): JSX.Element {
  const row = data.rows[index];
  data.prepareRow(row);
  return (
    <RowContainer
      {...row.getRowProps({
        style,
      })}
      className="tr"
    >
      {row.cells.map((cell: Cell<TableData<T>>) => {
        return (
          // eslint-disable-next-line react/jsx-key
          <CellContainer {...cell.getCellProps()} className="td">
            {cell.render('Cell')}
          </CellContainer>
        );
      })}
    </RowContainer>
  );
}

const RowContainer = styled.div`
  &:last-child {
    border-bottom: 0;
  }
  &:last-child .td {
    border-bottom: 0;
  }
`;

const CellContainer = styled.div`
  margin: 0;
  padding: 0.5rem;
  border-bottom: 1px solid black;

  &:last-child {
    border-right: 0;
  }
`;
