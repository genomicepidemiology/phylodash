import React from 'react';
import { Cell } from 'react-table';
import styled from 'styled-components';

import { MetadataValue } from '../../../types';
import { TableData } from '../types';

export function DescriptionCellRender<T extends MetadataValue>({ value }: Cell<TableData<T>>) {
  return (
    <div style={{ overflow: 'auto', width: '100%', height: '100%' }}>
      <span>{String(value)}</span>
    </div>
  );
}

export function VariantsCellRender<T extends MetadataValue>({ value }: Cell<TableData<T>>) {
  if (Array.isArray(value)) {
    return (
      <div style={{ overflow: 'auto', width: '100%', height: '100%' }}>
        {value.map((val, index) => {
          return <Variant key={index}>{val}</Variant>;
        })}
      </div>
    );
  }
  return <>{value}</>;
}

const Variant: React.FunctionComponent = ({
  children,
}: {
  children: React.ReactChildren;
}): JSX.Element => (
  <>
    <VariantSpan>{children}</VariantSpan>{' '}
  </>
);

const VariantSpan = styled.span`
  background: rgb(227, 227, 228);
  border-radius: 3px;
  color: rgb(64, 71, 83);
  padding: 0px 0.2em;
  margin: 1px 0;
  display: inline-block;
`;
