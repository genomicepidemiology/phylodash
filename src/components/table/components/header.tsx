import React from 'react';
import { HeaderGroup } from 'react-table';
import styled from 'styled-components';

import { MetadataValue } from '../../../types';
import { TableData } from '../types';

export function TableHeader<T extends MetadataValue>(props: {
  headerGroups: HeaderGroup<TableData<T>>[];
  children;
}): JSX.Element {
  return (
    <>
      {props.children}
      {props.headerGroups.map((headerGroup) => (
        // eslint-disable-next-line react/jsx-key
        <div {...headerGroup.getHeaderGroupProps()} className="tr">
          {headerGroup.headers.map((column) => (
            // eslint-disable-next-line react/jsx-key
            <TableCell {...column.getHeaderProps()}>
              <span {...column.getSortByToggleProps()}>
                {column.render('Header')}
                {/* Add a sort direction indicator */}
                {column.isSorted ? (column.isSortedDesc ? '🔽' : '🔼') : ''}
              </span>
              {/* Render the columns filter UI */}
              {column.canFilter ? column.render('Filter') : null}
            </TableCell>
          ))}
        </div>
      ))}
    </>
  );
}

const TableCell = styled.div`
  margin: 0;
  padding: 0.5rem;
  border-bottom: 1px solid black;
`;
