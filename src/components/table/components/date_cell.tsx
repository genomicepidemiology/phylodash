import { Cell } from 'react-table';

import { CustomDate, MetadataValue } from '../../../types';
import { TableData } from '../types';
import { formatCustomDate } from '../utils/custom_date_utils';

export function formatDateCell<T extends MetadataValue>({
  value,
}: Cell<TableData<T>, CustomDate>): string {
  return formatCustomDate(value);
}
