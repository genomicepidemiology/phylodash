/* eslint-disable react/prop-types */
import { Set as iSet } from 'immutable';
import { constants } from 'perf_hooks';
import React from 'react';
import Select, { components, createFilter } from 'react-select';
import { FilterProps } from 'react-table';
import { FixedSizeList } from 'react-window';
import styled from 'styled-components';

import { MetadataValue } from '../../../../types';
import { TableData } from '../../types';

type OptionType = { label: string | number; value: string | number };

const idFilterStyle = {
  valueContainer: (provided) => ({
    ...provided,
    overflow: 'auto',
    maxHeight: '40px',
  }),
  clearIndicator: (provided) => ({
    ...provided,
    padding: '0',
  }),
};

const DropdownIndicator = () => <></>;
const IndicatorSeparator = (props) => {
  if (!props.hasValue) {
    return <components.DropdownIndicator {...props} />;
  }
  return <></>;
};
// const CrossIcon = (props: any) => (<components.CrossIcon {...props} size={10}/>);
const SelectComponents = { DropdownIndicator, IndicatorSeparator, MenuList };

function MenuList(props) {
  const { options, children, maxHeight, getValue } = props;

  const height = 35;
  const [value] = getValue();
  const initialOffset = options.indexOf(value) * height;

  return (
    <FixedSizeList
      width="100%"
      height={maxHeight}
      itemCount={children.length}
      itemSize={height}
      initialScrollOffset={initialOffset}
      itemData={children}
    >
      {MultiSelectMenuRow}
    </FixedSizeList>
  );
}

const MultiSelectMenuRow = ({ data, index, style }) => <div style={style}>{data[index]}</div>;

const IDFilterPlaceholder = <>Select ID(s)</>;
const filterOptions = createFilter({ ignoreAccents: false });

export function IdMultiSelectFilter<T extends MetadataValue>({
  column: { filterValue, id, onChange },
  selectOptions,
}: FilterProps<TableData<T>>): JSX.Element {
  const options = React.useMemo(
    () =>
      selectOptions[id as string].map((variant: string) => {
        return {
          label: variant,
          value: variant,
        };
      }),
    [selectOptions, id]
  );

  const value = filterValue
    ? Array.from(filterValue).map((variant: string) => {
        return {
          label: variant,
          value: variant,
        };
      })
    : null;
  return (
    <Select
      value={value}
      onChange={(
        value: readonly {
          label: string;
          value: string;
        }[]
      ) => {
        const newFilterValue = value.length ? iSet(value.map((option) => option.value)) : undefined;
        // setFilter('variants', filterValue);
        onChange(newFilterValue);
      }}
      placeholder={IDFilterPlaceholder}
      styles={idFilterStyle}
      options={options}
      isMulti={true}
      components={SelectComponents}
      filterOption={filterOptions}
    />
  );
}

export const ANDPlaceholder = (
  <>
    Select...
    <br />
    AND filter
  </>
);

export const ORPlaceholder = (
  <>
    Select...
    <br />
    OR filter
  </>
);

export function createMultiSelect(placeholder: string) {
  return function MultiSelectFilter<T extends MetadataValue>({
    column: { filterValue, id },
    selectOptions,
    setFilter,
  }: FilterProps<TableData<T>>): JSX.Element {
    const options = React.useMemo(
      () =>
        selectOptions[id as string].map((variant: string | number) => {
          return {
            label: variant,
            value: variant,
          };
        }),
      [selectOptions, id]
    );

    return (
      <Select
        value={filterValue?.map((variant) => ({ label: variant, value: variant })) || null}
        onChange={(value: readonly OptionType[]) => {
          const newFilterValue = value.length ? value.map((option) => option.value) : undefined;
          setFilter(id, newFilterValue);
        }}
        options={options}
        isMulti={true}
        placeholder={placeholder}
        components={SelectComponents}
        filterOption={filterOptions}
      />
    );
  };
}

export function createVariantMultiSelect(placeholder: string) {
  return function MultiSelectFilter<T extends MetadataValue>({
    column: { filterValue, id },
    selectOptions,
    setFilter,
  }: FilterProps<TableData<T>>): JSX.Element {
    const [isAnd, setIsAnd] = React.useState(false);
    const options = React.useMemo(
      () =>
        selectOptions[id as string].map((variant: string | number) => {
          return {
            label: variant,
            value: variant,
          };
        }),
      [selectOptions, id]
    );

    const switchFilterType = () => {
      const newIsAnd = !isAnd;
      setIsAnd(newIsAnd);
      const type = newIsAnd ? 'and' : 'or';
      setFilter(id, { type, value: filterValue?.value ?? undefined });
    };

    return (
      <>
        <AndOrContainer>
          <AndOrButton onClick={switchFilterType}>
            <AndORField active={!isAnd} title="OR: Contains at least one selected mutation">
              OR
            </AndORField>
            <AndORField active={isAnd} title="AND: Contains all selected mutations">
              AND
            </AndORField>
          </AndOrButton>
        </AndOrContainer>
        <Select
          value={filterValue?.value?.map((variant) => ({ label: variant, value: variant })) || null}
          onChange={(value: readonly OptionType[]) => {
            const newFilterValue = value.length ? value.map((option) => option.value) : undefined;
            const type = isAnd ? 'and' : 'or';
            setFilter(id, { type, value: newFilterValue });
          }}
          options={options}
          isMulti={true}
          placeholder={placeholder}
          components={SelectComponents}
          filterOption={filterOptions}
        />
      </>
    );
  };
}

const AndOrContainer = styled.div`
  display: inline-block;
  font-size: 12px;
  margin: 4px;
  border-radius: 4px;
  border: 2px solid #bebdbd;
  background: #fff;
  width: fit-content;
`;

const AndOrButton = styled.button<{ active?: boolean }>`
  width: 60px;
  height: 100%;
  display: block;
  padding: 0;
  outline: none;
  border: 0;
  box-sizing: border-box;
  background-color: transparent;
  cursor: pointer;
`;

const AndORField = styled.span<{ active?: boolean }>`
  display: inline-block;
  width: 30px;
  height: 100%;
  background-color: ${(props) => (props.active ? 'transparent' : '#bebdbd')};
  color: ${(props) => (props.active ? '#000' : '#7e7e7e')};
`;
