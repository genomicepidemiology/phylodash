import React from 'react';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { LocaleUtils } from 'react-day-picker/types/LocaleUtils';
import { DayPickerInputProps, DayPickerProps } from 'react-day-picker/types/Props';
import { FilterProps } from 'react-table';
import styled from 'styled-components';

import { MetadataValue } from '../../../../types';
import { TableData } from '../../types';
import { getRecordDateRange } from '../../utils/get_row_date_range';

export function DateRangeFilter<T extends MetadataValue>({
  column: { filterValue = { start: null, end: null }, setFilter },
  preFilteredRows,
}: FilterProps<TableData<T>>) {
  const toDateRef = React.useRef<DayPickerInput>(null);
  const { minDate, maxDate } = React.useMemo(
    () => getRecordDateRange(preFilteredRows),
    [preFilteredRows]
  );

  return (
    <DateInputGrid>
      <StartDateArea>
        <div>from</div>

        <RefCustomDayPickerInput
          value={filterValue.start}
          onDayChange={(date) => {
            const end = filterValue.end && filterValue.end > date ? filterValue.end : null;
            setFilter({ start: date, end });
          }}
          dateRange={{ from: new Date(minDate), to: new Date(maxDate) }}
          dayPickerProps={{
            onDayClick: () => {
              toDateRef.current.getInput().focus();
            },
          }}
        />
      </StartDateArea>

      <EndDateArea>
        <div>to</div>
        <RefCustomDayPickerInput
          ref={toDateRef}
          value={filterValue.end}
          onDayChange={(date) => {
            setFilter({ ...filterValue, end: date });
          }}
          dayPickerProps={{
            disabledDays: { before: filterValue.start },
            fromMonth: filterValue.start || new Date(minDate),
          }}
          dateRange={{ from: filterValue?.start || new Date(minDate), to: new Date(maxDate) }}
        />
      </EndDateArea>
    </DateInputGrid>
  );
}

const DateInputGrid = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  grid-template-areas: 'startDate endDate';
`;

const StartDateArea = styled.div`
  grid-area: startDate;
`;

const EndDateArea = styled.div`
  grid-area: endDate;
`;

type CustomDayPickerInputProps = {
  value: DayPickerInputProps['value'];
  dateRange: { from: Date; to: Date };
  dayPickerProps?: DayPickerProps;
  onDayChange: DayPickerInputProps['onDayChange'];
};

const RefCustomDayPickerInput = React.forwardRef(CustomDayPickerInput);

function CustomDayPickerInput(
  { value, dateRange, dayPickerProps, onDayChange }: CustomDayPickerInputProps,
  ref
) {
  const [month, setMonth] = React.useState(dateRange.from);
  if (dateRange.from > month) setMonth(dateRange.from);

  return (
    <>
      <DayPickerInput
        // component={(props) => <Input ref={ref} {...props}/>}
        ref={ref}
        value={value}
        placeholder="YYYY-M-D"
        onDayChange={onDayChange}
        dayPickerProps={{
          captionElement: captionElementFactory(dateRange.from, dateRange.to, (date) => {
            setMonth(date);
          }),
          month: month,
          fromMonth: dateRange.from,
          toMonth: dateRange.to,
          ...dayPickerProps,
        }}
        inputProps={{ style: dayPickerInputStyle }}
      />
    </>
  );
}

const dayPickerInputStyle = {
  width: '85px',
  backgroundColor: 'hsl(0, 0%, 100%)',
  borderColor: 'hsl(0, 0%, 80%)',
  borderRadius: '4px',
  borderStyle: 'solid',
  borderWidth: '1px',
  outline: '0 !important',
  padding: '2px 8px',
};

function captionElementFactory(fromMonth: Date, toMonth: Date, onChange: (date: Date) => void) {
  // eslint-disable-next-line react/prop-types
  const YearMonthDropDown = ({ date, localeUtils }) => (
    <YearMonthForm
      currentMonthYear={date}
      localeUtils={localeUtils}
      fromMonth={fromMonth}
      toMonth={toMonth}
      onChange={onChange}
    />
  );
  return YearMonthDropDown;
}

type YearMonthFormProps = {
  fromMonth: Date;
  toMonth: Date;
  currentMonthYear: Date;
  localeUtils: LocaleUtils;
  onChange: (date) => void;
};

function YearMonthForm({
  fromMonth,
  toMonth,
  currentMonthYear,
  localeUtils,
  onChange,
}: YearMonthFormProps) {
  const months = localeUtils.getMonths();

  const years = [];
  for (let i = fromMonth.getFullYear(); i <= toMonth.getFullYear(); i += 1) {
    years.push(i);
  }

  const handleChange = function handleChange(e) {
    const { year, month } = e.target.form;
    onChange(new Date(year.value, month.value));
  };

  return (
    <form className="DayPicker-Caption">
      <select name="month" onChange={handleChange} value={currentMonthYear.getMonth()}>
        {months.map((month, i) => (
          <option key={month} value={i}>
            {month}
          </option>
        ))}
      </select>
      <select name="year" onChange={handleChange} value={currentMonthYear.getFullYear()}>
        {years.map((year) => (
          <option key={year} value={year}>
            {year}
          </option>
        ))}
      </select>
    </form>
  );
}
