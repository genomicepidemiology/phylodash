import React from 'react';
import { FilterProps } from 'react-table';
import styled from 'styled-components';

import { MetadataValue } from '../../../../types';
import { TableData } from '../../types';

// Define a default UI for filtering
export function DefaultColumnFilter<T extends MetadataValue>({
  column: { filterValue, setFilter },
}: FilterProps<TableData<T>>) {
  // const count = preFilteredRows.length;
  return (
    <DefaultInputField
      value={filterValue || ''}
      onChange={(e) => {
        setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
      }}
      placeholder="search"
      style={{ width: '100%' }}
    />
  );
}

const DefaultInputField = styled.input`
  background-color: hsl(0, 0%, 100%);
  border-color: hsl(0, 0%, 80%);
  border-radius: 4px;
  border-style: solid;
  border-width: 1px;
  min-height: 38px;
  outline: 0 !important;
  padding: 2px 8px;
`;
