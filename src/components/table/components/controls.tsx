import { Set as iSet } from 'immutable';
import React from 'react';
import { TableInstance } from 'react-table';
import styled from 'styled-components';

import { MetadataValue } from '../../../types';
import { TableData, TableProps, TableActions, TableControlsActions } from '../types';

type ControlProps<T extends MetadataValue> = { subtreeBaseLeaf: string, areFiltersActive: boolean } & TableControlsActions &
  Pick<TableInstance<TableData<T>>, 'setAllFilters'> &
  Pick<TableProps<T>, 'selectedIDs'> &
  Pick<TableActions, 'toggleAllRowsSelected'>;

export function Controls<T extends MetadataValue>({
  selectedIDs,
  subtreeBaseLeaf,
  areFiltersActive,
  setAllFilters,
  onIDChange,
  toggleAllRowsSelected,
  onShowOrigTree,
  onDownloadClick,
  onShowSubtree,
}: ControlProps<T>): JSX.Element {
  const noRows = selectedIDs.size;
  const plural = noRows > 1 ? 's' : '';
  return (
    <ControlContainer className="tr">
      <SelectCount>
        {noRows} row{plural} selected
      </SelectCount>
      <Button
        disabled={selectedIDs.size === 0}
        onClick={() => {
          toggleAllRowsSelected(false);
        }}
      >
        Deselect all
      </Button>
      <Button
        disabled={!areFiltersActive}
        onClick={() => {
          setAllFilters([]);
          onIDChange(iSet());
        }}
      >
        Reset Filters
      </Button>
      {onDownloadClick ? (
        <Button
          disabled={selectedIDs.size === 0}
          onClick={() => {
            onDownloadClick(selectedIDs);
          }}
        >
          Download selected
        </Button>
      ) : null}

      <Button
        disabled={selectedIDs.size !== 1 && subtreeBaseLeaf === undefined}
        onClick={() => {
          const leafID = selectedIDs.values().next().value;
          if (subtreeBaseLeaf !== undefined) onShowOrigTree();
          else onShowSubtree(leafID);
        }}
      >
        {subtreeBaseLeaf ? 'Show original tree' : 'Show sample context'}
      </Button>
    </ControlContainer>
  );
}

const SelectCount = styled.div`
  width: 11rem;
  display: inline-block;
`;

const ControlContainer = styled.div`
  border: solid #000;
  border-width: 0 0 1px;
  padding: 3px 6px;
`;

const Button = styled.button`
  margin: 0 0 0 6px;
`;
