import React from 'react';
import { useTable, useFilters, useSortBy, useBlockLayout, Row } from 'react-table';
import { FixedSizeList } from 'react-window';
import styled from 'styled-components';

import { MetadataValue } from '../../types';
import { TableHelp } from '../help/helpTooltips';
import { checkBoxUIHook } from './components/checkboxUI';
import { Controls } from './components/controls';
import { DefaultColumnFilter } from './components/filters/default';
import { TableHeader } from './components/header';
import { RowRender } from './components/row';
import { createUseSelectHook } from './hooks/useControlledRowSelect';
import { TableData, TableProps } from './types';
import { filterTypes } from './utils/filter_types';
import { updateIdFilter } from './utils/update_id_filter';

export default function Table<T extends MetadataValue>({
  columnsDef,
  data,
  idFilterVal,
  selectOptions,
  selectedIDs,
  subtreeBaseLeaf,
  showHelp,
  actions: { toggleRowSelected, toggleAllRowsSelected, toggleAllVisibleRowRowSelect, controls },
}: TableProps<T>): JSX.Element {
  

  const tableWrapper = React.useRef(null);
  const tableHeader = React.useRef(null);
  const [height, setHeight] = React.useState(200);

  React.useEffect(() => {
    window.addEventListener('resize', updateHeight);
    updateHeight();
    // clean up
    return () => window.removeEventListener('resize', updateHeight);
  }, []);

  const updateHeight = () => {
    if (tableWrapper.current && tableHeader.current) {
      const tableHeight = tableWrapper.current.clientHeight;
      const headerHeight = tableHeader.current.clientHeight;
      const tbodyHeight = tableHeight - headerHeight;
      setHeight(tbodyHeight);
    }
  };

  const defaultColumn = React.useMemo(
    () => ({
      // Let's set up our default Filter UI
      Filter: DefaultColumnFilter,
      width: 120,
    }),
    []
  );

  const controledSelect = React.useMemo(
    () =>
      createUseSelectHook(toggleRowSelected, toggleAllRowsSelected, toggleAllVisibleRowRowSelect),
    [toggleRowSelected, toggleAllRowsSelected, toggleAllVisibleRowRowSelect]
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    state,
    totalColumnsWidth,
    prepareRow,
    setAllFilters,
  } = useTable<TableData<T>>(
    {
      columns: columnsDef,
      data,
      initialState: { hiddenColumns: ['longitude', 'latitude'] },
      autoResetHiddenColumns: false,
      defaultColumn,
      // autoResetSelectedRows: false,
      filterTypes,
      getRowId,
      selectOptions,
      useControlledState: (state) => {
        const filters = React.useMemo(
          () => updateIdFilter(state, idFilterVal),
          // eslint-disable-next-line react-hooks/exhaustive-deps
          [state, idFilterVal]
        );
        return React.useMemo(() => {
          return {
            ...state,
            filters,
            selectedRows: selectedIDs,
          };
          // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [state, idFilterVal, selectedIDs]);
      },
    },
    useFilters,
    useSortBy,
    controledSelect,
    checkBoxUIHook,
    useBlockLayout
  );

  const itemData = {
    rows,
    prepareRow,
  };

  const areFiltersActive = React.useMemo(
    () => Boolean(state.filters.length) || Boolean(idFilterVal?.size),
    [state.filters, idFilterVal]
  );

  return (
    <>
      <TableContainer {...getTableProps()} ref={tableWrapper}>
        <div ref={tableHeader} style={{ width: 'fit-content' }}>
          <TableHeader headerGroups={headerGroups}>
            <Controls
              areFiltersActive={areFiltersActive}
              selectedIDs={selectedIDs}
              subtreeBaseLeaf={subtreeBaseLeaf}
              onShowSubtree={controls.onShowSubtree}
              onIDChange={controls.onIDChange}
              onShowOrigTree={controls.onShowOrigTree}
              onDownloadClick={controls.onDownloadClick}
              setAllFilters={setAllFilters}
              toggleAllRowsSelected={toggleAllRowsSelected}
            />
          </TableHeader>
        </div>
        <div {...getTableBodyProps()}>
          <FixedSizeList<{
            rows: Row<TableData<T>>[];
            prepareRow: (row: Row<TableData<T>>) => void;
          }>
            height={height}
            width={totalColumnsWidth}
            itemCount={rows.length}
            itemSize={150}
            itemData={itemData}
          >
            {RowRender}
          </FixedSizeList>
        </div>
        <TableHelp visible={showHelp} />
      </TableContainer>
    </>
  );
}

const TableContainer = styled.div`
  grid-area: metadata;
  /* background: #c9c9c8; */
  overflow: scroll;
  position: relative;
  display: inline-block;
  border: 1px solid black;
`;

// use Accession id of samples instead of index
const getRowId = (row, index, parent) =>
  `${parent ? [parent.id, index].join('.') : row ? row.id : index}`;
