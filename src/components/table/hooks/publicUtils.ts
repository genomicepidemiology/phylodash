import React from 'react';

function mergeProps(...propList) {
  return propList.reduce((props, next) => {
    const { style, className, ...rest } = next;

    const new_props = {
      ...props,
      ...rest,
    };

    if (style) {
      new_props.style = new_props.style ? { ...(new_props.style || {}), ...(style || {}) } : style;
    }

    if (className) {
      new_props.className = new_props.className ? new_props.className + ' ' + className : className;
    }

    if (new_props.className === '') {
      delete new_props.className;
    }

    return new_props;
  }, {});
}

function handlePropGetter(prevProps, userProps, meta = null) {
  // Handle a lambda, pass it the previous props
  if (typeof userProps === 'function') {
    return handlePropGetter({}, userProps(prevProps, meta));
  }

  // Handle an array, merge each item as separate props
  if (Array.isArray(userProps)) {
    return mergeProps(prevProps, ...userProps);
  }

  // Handle an object by default, merge the two objects
  return mergeProps(prevProps, userProps);
}

export const makePropGetter = (hooks, meta = {}) => {
  return (userProps = {}) =>
    [...hooks, userProps].reduce(
      (prev, next) =>
        handlePropGetter(prev, next, {
          ...meta,
          userProps,
        }),
      {}
    );
};

export function ensurePluginOrder(plugins, befores, pluginName, afters = null) {
  if (process.env.NODE_ENV !== 'production' && afters) {
    throw new Error(
      `Defining plugins in the "after" section of ensurePluginOrder is no longer supported (see plugin ${pluginName})`
    );
  }
  const pluginIndex = plugins.findIndex((plugin) => plugin.pluginName === pluginName);

  if (pluginIndex === -1) {
    if (process.env.NODE_ENV !== 'production') {
      throw new Error(`The plugin "${pluginName}" was not found in the plugin list!
This usually means you need to need to name your plugin hook by setting the 'pluginName' property of the hook function, eg:

  ${pluginName}.pluginName = '${pluginName}'
`);
    }
  }

  befores.forEach((before) => {
    const beforeIndex = plugins.findIndex((plugin) => plugin.pluginName === before);
    if (beforeIndex > -1 && beforeIndex > pluginIndex) {
      if (process.env.NODE_ENV !== 'production') {
        throw new Error(
          `React Table: The ${pluginName} plugin hook must be placed after the ${before} plugin hook!`
        );
      }
    }
  });
}

export function useGetLatest(obj) {
  const ref = React.useRef();
  ref.current = obj;

  return React.useCallback(() => ref.current, []);
}
