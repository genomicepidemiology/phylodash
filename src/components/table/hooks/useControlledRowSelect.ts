import { Set as iSet } from 'immutable';
import { TableInstance } from 'react-table';

import { TableActions } from '../types';
import { makePropGetter, ensurePluginOrder, useGetLatest } from './publicUtils';

const pluginName = 'useControlledRowSelect';

export function createUseSelectHook(
  toggleRowSelected: TableActions['toggleRowSelected'],
  toggleAllRowsSelected,
  toggleAllVisibleRowsSelected
) {
  const useRowSelect = (hooks) => {
    hooks.getToggleRowSelectedProps = [defaultGetToggleRowSelectedProps];
    hooks.getToggleAllRowsSelectedProps = [defaultGetToggleAllRowsSelectedProps];
    hooks.getToggleAllVisibleRowsSelectedProps = [defaultGetToggleAllVisibleRowsSelectedProps];
    hooks.useInstance.push(
      getUseInstance(toggleRowSelected, toggleAllRowsSelected, toggleAllVisibleRowsSelected)
    );
    hooks.prepareRow.push(prepareRow);
    hooks.stateReducers.push(reducer);
  };
  useRowSelect.pluginName = pluginName;
  return useRowSelect;
}

const defaultGetToggleRowSelectedProps = (props, { instance, row }) => {
  const checked = instance.state.selectedRows.has(row.id);

  return [
    props,
    {
      onChange: (e) => {
        row.toggleRowSelected(e.target.checked);
      },
      style: {
        cursor: 'pointer',
      },
      checked,
      title: 'Toggle Row Selected',
      indeterminate: row.isSomeSelected,
    },
  ];
};

const defaultGetToggleAllRowsSelectedProps = (props, { instance }: { instance: TableInstance }) => [
  props,
  {
    onChange: (e) => {
      instance.toggleAllRowsSelected(e.target.checked);
    },
    style: {
      cursor: 'pointer',
    },
    checked: instance.initialRows.length === instance.state.selectedRows.size,
    title: 'Toggle All Rows Selected',
    indeterminate: Boolean(
      instance.rows.length !== instance.state.selectedRows.size && instance.state.selectedRows.size
    ),
  },
];

const defaultGetToggleAllVisibleRowsSelectedProps = (
  props,
  { instance }: { instance: TableInstance }
) => {
  const ids = Object.keys(instance.rowsById);
  const checked = instance.state.selectedRows.equals(iSet(ids));
  return [
    props,
    {
      onChange: (e) => {
        instance.toggleAllVisibleRowsSelected(ids, e.target.checked);
      },
      style: {
        cursor: 'pointer',
      },
      checked,
      title: 'Toggle All Rows Selected',
      indeterminate: Boolean(!checked && instance.state.selectedRows.size > 0),
    },
  ];
};

const actions = {
  init: 'init',
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function reducer(state, action, previousState, instance) {
  if (action.type === actions.init) {
    return {
      selectedRows: iSet(),
      ...state,
    };
  }
  return state;
}

function getUseInstance(toggleRowSelected, toggleAllRowsSelected, toggleAllVisibleRowsSelected) {
  return function useInstance(instance: TableInstance) {
    const {
      getHooks,
      plugins,
      // rowsById,
      // nonGroupedRowsById = rowsById,
      // autoResetSelectedRows = true,
      // state: { selectedRows },
    } = instance;

    ensurePluginOrder(
      plugins,
      ['useFilters', 'useGroupBy', 'useSortBy', 'useExpanded', 'usePagination'],
      pluginName
    );

    const getInstance = useGetLatest(instance);

    const getToggleAllRowsSelectedProps = makePropGetter(getHooks().getToggleAllRowsSelectedProps, {
      instance: getInstance(),
    });

    const getToggleAllVisibleRowsSelectedProps = makePropGetter(
      getHooks().getToggleAllVisibleRowsSelectedProps,
      { instance: getInstance() }
    );

    Object.assign(instance, {
      toggleRowSelected,
      toggleAllRowsSelected,
      toggleAllVisibleRowsSelected,
      getToggleAllRowsSelectedProps,
      getToggleAllVisibleRowsSelectedProps,
    });
  };
}

function prepareRow(row, { instance }) {
  row.toggleRowSelected = (set) => instance.toggleRowSelected(row.id, set);

  row.getToggleRowSelectedProps = makePropGetter(instance.getHooks().getToggleRowSelectedProps, {
    instance: instance,
    row,
  });
}
