/* eslint-disable @typescript-eslint/consistent-type-definitions */
import { Set as iSet } from 'immutable';
import {
  // UseColumnOrderInstanceProps,
  // UseColumnOrderState,
  // UseExpandedHooks,
  // UseExpandedInstanceProps,
  // UseExpandedOptions,
  // UseExpandedRowProps,
  // UseExpandedState,
  UseFiltersColumnOptions,
  UseFiltersColumnProps,
  UseFiltersInstanceProps,
  UseFiltersOptions,
  UseFiltersState,
  // UseGlobalFiltersColumnOptions,
  // UseGlobalFiltersInstanceProps,
  // UseGlobalFiltersOptions,
  // UseGlobalFiltersState,
  // UseGroupByCellProps,
  // UseGroupByColumnOptions,
  // UseGroupByColumnProps,
  // UseGroupByHooks,
  // UseGroupByInstanceProps,
  // UseGroupByOptions,
  // UseGroupByRowProps,
  // UseGroupByState,
  // UsePaginationInstanceProps,
  // UsePaginationOptions,
  // UsePaginationState,
  // UseResizeColumnsColumnOptions,
  // UseResizeColumnsColumnProps,
  // UseResizeColumnsOptions,
  // UseResizeColumnsState,
  // UseRowSelectHooks,
  // UseRowSelectInstanceProps,
  // UseRowSelectOptions,
  // UseRowSelectRowProps,
  // UseRowSelectState,
  // UseRowStateCellProps,
  // UseRowStateInstanceProps,
  // UseRowStateOptions,
  // UseRowStateRowProps,
  // UseRowStateState,
  UseSortByColumnOptions,
  UseSortByColumnProps,
  UseSortByHooks,
  UseSortByInstanceProps,
  UseSortByOptions,
  UseSortByState,
  // utils
  TableToggleAllRowsSelectedProps,
  TableToggleRowsSelectedProps,
  PropGetter,
  IdType,
} from 'react-table';

import { SelectOptions } from './table/types';

export interface UseControlledRowSelectState<D extends Record<string, unknown>> {
  selectedRows: iSet<IdType<D>>;
}

export type UseControledRowSelectInstanceProps<D extends Record<string, unknown>> = {
  toggleRowSelected: (rowId: IdType<D>, set?: boolean) => void;
  toggleAllRowsSelected: (value?: boolean) => void;
  toggleAllVisibleRowsSelected: (ids: string[], value: boolean) => void;
  getToggleAllRowsSelectedProps: (
    props?: Partial<TableToggleAllRowsSelectedProps>
  ) => TableToggleAllRowsSelectedProps;
  getToggleAllVisibleRowsSelectedProps: (
    props?: Partial<TableToggleAllRowsSelectedProps>
  ) => TableToggleAllRowsSelectedProps;
};

export interface UseControlledRowSelectHooks<D extends Record<string, unknown>> {
  getToggleRowSelectedProps: PropGetter<D, TableToggleRowsSelectedProps>[];
  getToggleAllRowsSelectedProps: PropGetter<D, TableToggleAllRowsSelectedProps>[];
  getToggleAllVisibleRowsSelectedProps: PropGetter<D, TableToggleAllRowsSelectedProps>[];
}

export interface UseControlledRowSelectRowProps {
  isSelected: boolean;
  toggleRowSelected: (set?: boolean) => void;
  getToggleRowSelectedProps: (
    props?: Partial<TableToggleRowsSelectedProps>
  ) => TableToggleRowsSelectedProps;
}

declare module 'react-table' {
  // take this file as-is, or comment out the sections that don't apply to your plugin configuration

  export interface TableOptions<D extends Record<string, unknown>>
    // UseExpandedOptions<D>,
    extends UseFiltersOptions<D>,
      // UseGlobalFiltersOptions<D>,
      // UseGroupByOptions<D>,
      // UsePaginationOptions<D>,
      // UseResizeColumnsOptions<D>,
      // UseRowSelectOptions<D>,
      // UseRowStateOptions<D>,
      UseSortByOptions<D>,
      Record<'selectOptions', SelectOptions> {
    // note that having Record here allows you to add anything to the options, this matches the spirit of the
    // underlying js library, but might be cleaner if it's replaced by a more specific type that matches your
    // feature set, this is a safe default.
    // Record<string, any>
  }

  export interface Hooks<D extends Record<string, unknown> = Record<string, unknown>>
    // UseExpandedHooks<D>,
    // UseGroupByHooks<D>,
    extends UseControlledRowSelectHooks<D>,
      UseSortByHooks<D> {}

  export interface TableInstance<D extends Record<string, unknown> = Record<string, unknown>>
    // UseColumnOrderInstanceProps<D>,
    // UseExpandedInstanceProps<D>,
    extends UseFiltersInstanceProps<D>,
      // UseGlobalFiltersInstanceProps<D>,
      // UseGroupByInstanceProps<D>,
      // UsePaginationInstanceProps<D>,
      UseControledRowSelectInstanceProps<D>,
      // UseRowStateInstanceProps<D>,
      UseSortByInstanceProps<D>,
      Required<{ initialRows: Row<D>[] }> {}

  export interface TableState<D extends Record<string, unknown> = Record<string, unknown>>
    // UseColumnOrderState<D>,
    // UseExpandedState<D>,
    extends UseFiltersState<D>,
      // UseGlobalFiltersState<D>,
      // UseGroupByState<D>,
      // UsePaginationState<D>,
      // UseResizeColumnsState<D>,
      UseControlledRowSelectState<D>,
      // UseRowStateState<D>,
      UseSortByState<D> {}

  export interface ColumnInterface<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseFiltersColumnOptions<D>,
      // UseGlobalFiltersColumnOptions<D>,
      // UseGroupByColumnOptions<D>,
      // UseResizeColumnsColumnOptions<D>,
      Partial<{ onChange: (ids: (string | number)[] | iSet<string>) => void }>,
      UseSortByColumnOptions<D> {}

  export interface ColumnInstance<
    D extends Record<string, unknown> = Record<string, unknown>
  > extends UseFiltersColumnProps<D>,
      // UseGroupByColumnProps<D>,
      // UseResizeColumnsColumnProps<D>,
      UseSortByColumnProps<D> {}

  // export interface Cell<D extends Record<string, unknown> = Record<string, unknown>, V = any>
  //   extends
  //   UseGroupByCellProps<D>,
  //   UseRowStateCellProps<D>
  //   {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  export interface Row<D extends Record<string, unknown> = Record<string, unknown>>
    // UseExpandedRowProps<D>,
    // UseGroupByRowProps<D>,
    // UseRowSelectRowProps<D>,
    extends UseControlledRowSelectRowProps {
    // UseRowStateRowProps<D>
  }
}
