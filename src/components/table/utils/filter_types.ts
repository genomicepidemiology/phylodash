import { Set as iSet } from 'immutable';
// import { DefaultFilterTypes, FilterType } from 'react-table';

// import { MetadataValue } from '../../types';
// import { TableData } from '../table';

// type FilterTypes<T extends MetadataValue> = { [key: string]: FilterType<TableData<T>> | DefaultFilterTypes | string };

const idFilterFn = (rows, ids, filterValue: iSet<string>) => {
  if (filterValue && filterValue.size > 0) {
    return rows.filter((row) => {
      return ids.some((id) => {
        const rowValue = row.values[id];
        return filterValue.has(rowValue);
      });
    });
  }
  return rows;
};

idFilterFn.autoRemove = (val: unknown) => !val;

export const filterTypes = {
  id: idFilterFn,
};

export const customDateRangeFn = (rows, ids, filterValue: { start: Date; end: Date }) => {
  const { start, end } = filterValue;
  const normStart = start && new Date(start.getFullYear(), start.getMonth(), start.getDate());
  const normEnd = end && new Date(end.getFullYear(), end.getMonth(), end.getDate());

  return rows.filter((row) => {
    return ids.some((id) => {
      const rowValue = row.values[id];
      const rowDate = new Date(
        rowValue.year,
        rowValue?.month ? rowValue?.month - 1 : 0,
        rowValue?.day
      );

      // prettier-ignore
      const result = (normStart && normEnd) ? rowDate >= normStart && rowDate <= normEnd
      : normStart ? rowDate >= normStart
      : rowDate <= normEnd;
      return result;
    });
  });
};

customDateRangeFn.autoRemove = (val) => {
  return !val || (!val?.start && !val?.end);
};

export const variantFilterFn = (
  rows,
  ids,
  filterValue: { type: 'and' | 'or'; value: (string | number)[] }
) => {
  if (filterValue && filterValue.value && filterValue.value.length > 0) {
    if (filterValue.type === 'and') {
      return includesAll(rows, ids, filterValue.value);
    } else if (filterValue.type === 'or') {
      return includesSome(rows, ids, filterValue.value);
    }
  }
  return rows;
};

variantFilterFn.autoRemove = (val: unknown) => !val;

const includesAll = (rows, ids, filterValue) => {
  return rows.filter((row) => {
    return ids.some((id) => {
      const rowValue = row.values[id];
      return rowValue && rowValue.length && filterValue.every((val) => rowValue.includes(val));
    });
  });
};

includesAll.autoRemove = (val) => !val || !val.length;

const includesSome = (rows, ids, filterValue) => {
  return rows.filter((row) => {
    return ids.some((id) => {
      const rowValue = row.values[id];
      return rowValue && rowValue.length && filterValue.some((val) => rowValue.includes(val));
    });
  });
};

includesSome.autoRemove = (val) => !val || !val.length;
