import { Column } from 'react-table';

import { MetadataValue, ColumnDef } from '../../../types';
import { DescriptionCellRender, VariantsCellRender } from '../components/custom_cells';
import { formatDateCell } from '../components/date_cell';
import { DateRangeFilter } from '../components/filters/date_range';
import {
  createMultiSelect,
  IdMultiSelectFilter,
  createVariantMultiSelect,
} from '../components/filters/multiselect';
import { TableData } from '../types';
import { sortDate } from './date_sort';
import { customDateRangeFn, variantFilterFn } from './filter_types';

const defaultColumnsParams = {
  id: {
    Filter: IdMultiSelectFilter,
    filter: 'id',
    width: 160,
  },
  description: {
    Cell: DescriptionCellRender,
    width: 240,
  },
  date: {
    Cell: formatDateCell,
    width: 190,
    Filter: DateRangeFilter,
    filter: customDateRangeFn,
    sortType: sortDate,
  },
  variants: {
    Cell: VariantsCellRender,
    width: 160,
    Filter: createVariantMultiSelect('Select one/multiple'),
    filter: variantFilterFn,
  },
  country: {
    width: 220,
    Filter: createMultiSelect('Select one/multiple'),
    filter: 'includesValue',
  },
  'PANGO-lineage': {
    width: 160,
    Filter: createMultiSelect('Select one/multiple'),
    filter: 'includesValue',
  },
};

export function createTableColumnsDef<T extends MetadataValue>(headers: ColumnDef[], onIDChange) {
  const columns = headers.map((columnDef: ColumnDef) => {
    const id = columnDef.id;
    const column: Column<TableData<T>> = {
      Header: id,
      accessor: id,
      ...defaultColumnsParams[id],
      ...columnDef,
    };
    if (id === 'id') {
      column.onChange = onIDChange;
    }
    return column;
  });

  return columns;
}
