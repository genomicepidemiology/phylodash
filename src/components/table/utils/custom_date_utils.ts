import { CustomDate } from '../../../types';

export function compareDateStrings(aDate: string, bDate: string): -1 | 0 | 1 {
  // prettier-ignore
  const result = 
      aDate > bDate ? 1
    : bDate > aDate ? -1
    : 0;
  return result;
}

export function formatCustomDate({ year, month, day }: CustomDate): string {
  const paddedMonth = month ? month.toString().padStart(2, '0') : null;
  const paddedDay = day ? day.toString().padStart(2, '0') : null;
  // prettier-ignore
  // day && month && day > 9 ? `${year}-${month}-${day}` :
  return day && month ? `${year}-${paddedMonth}-${paddedDay}` 
    : month ? `${year}-${paddedMonth}` 
    : `${year}`;
}
