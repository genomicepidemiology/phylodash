import { formatCustomDate, compareDateStrings } from './custom_date_utils';

test('formatCustomDate', () => {
  const aYearDate = { year: 2019 };
  expect(formatCustomDate(aYearDate)).toBe(aYearDate.year.toString());
  const aYearMonthDate = { year: 2019, month: 2 };
  expect(formatCustomDate(aYearMonthDate)).toBe(`${aYearMonthDate.year}-02`);
  const aYearMonthDayDate = { year: 2019, month: 1, day: 31 };
  expect(formatCustomDate(aYearMonthDayDate)).toBe(`${aYearMonthDayDate.year}-01-31`);
});

test('formatCustomDate 2', () => {
  const aYearDate = { year: 2019 };
  expect(formatCustomDate(aYearDate)).toBe(aYearDate.year.toString());
  const aYearMonthDate = { year: 2019, month: 2 };
  expect(formatCustomDate(aYearMonthDate)).toBe(`${aYearMonthDate.year}-02`);
  const aYearMonthDayDate = { year: 2019, month: 1, day: 5 };
  expect(formatCustomDate(aYearMonthDayDate)).toBe(`${aYearMonthDayDate.year}-01-05`);
});

const compareDateStringTestDate = [
  ['2020', '2019', 1],
  ['2020', '2021', -1],
  ['2020', '2020', 0],
  ['2021-01', '2020-01', 1],
  ['2020-10', '2020-01', 1],
  ['2021-05', '2020-10', 1],
  ['2020-01', '2020-01', 0],
  ['2019-01', '2020-01', -1],
  ['2020-01', '2020-10', -1],
  ['2019-10', '2020-01', -1],
  ['2020-05', '2020-11', -1],
  ['2021-01-31', '2020-01-31', 1],
  ['2021-01-31', '2020-10-31', 1],
  ['2021-01-30', '2020-01-31', 1],
  ['2021-01-30', '2020-10-31', 1],
  ['2020-10-31', '2020-01-31', 1],
  ['2020-10-30', '2020-01-31', 1],
  ['2020-10-31', '2020-01-30', 1],
  ['2020-01-31', '2020-01-30', 1],
  ['2020-01-10', '2020-01-01', 1],
  ['2020-01-10', '2020-01-09', 1],
  ['2020-01-31', '2020-01-31', 0],
  ['2020-01-31', '2021-01-31', -1],
  ['2020-10-31', '2021-01-31', -1],
  ['2020-01-31', '2021-01-30', -1],
  ['2020-10-31', '2021-01-30', -1],
  ['2020-01-31', '2020-10-31', -1],
  ['2020-01-31', '2020-10-30', -1],
  ['2020-01-30', '2020-10-31', -1],
  ['2020-01-30', '2020-01-31', -1],
  ['2020-01-01', '2020-01-10', -1],
  ['2020', '2019-12-03', 1],
];

test.each(compareDateStringTestDate)(
  'compareDateStrings(%s, %s)',
  (a: string, b: string, expected: number) => {
    expect(compareDateStrings(a, b)).toBe(expected);
  }
);
