import { Row } from 'react-table';

import { MetadataValue } from '../../../types';
import { TableData } from '../types';

export function sortDate<T extends MetadataValue>(
  rowA: Row<TableData<T>>,
  rowB: Row<TableData<T>>,
  columnId: string
): 1 | 0 | -1 {
  const [cDateA, cDateB] = getRowValuesByColumnID(rowA, rowB, columnId);

  const zeroIndexMonthA = cDateA?.month ? cDateA?.month - 1 : 0;
  const dateA = new Date(cDateA.year, zeroIndexMonthA, cDateA?.day);

  const zeroIndexMonthB = cDateB?.month ? cDateB?.month - 1 : 0;
  const dateB = new Date(cDateB.year, zeroIndexMonthB, cDateB?.day);

  // prettier-ignore
  return dateA > dateB ? 1
    : dateA < dateB ? -1 
    : 0;
}
function getRowValuesByColumnID(row1, row2, columnId) {
  return [row1.values[columnId], row2.values[columnId]];
}
