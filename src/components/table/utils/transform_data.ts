import { Set as iSet } from 'immutable';

import { Metadata, MetadataValue } from '../../../types';
import { SelectOptions, TableData } from '../types';

export function arrayTableData<T extends MetadataValue>(
  metadata: Metadata<T>,
  workingIDs: iSet<string>
): [TableData<T>[], SelectOptions] {
  const tableData: TableData<T>[] = [];
  const variants = new Set<string>();
  const countries = new Set<string>();
  const lineages = new Set<string>();
  for (const key in metadata) {
    if (workingIDs.size === 0 || workingIDs.has(key)) {
      tableData.push({ id: key, ...metadata[key] });
      if ('variants' in metadata[key] && metadata[key].variants) {
        metadata[key].variants.forEach((variant) => {
          variants.add(variant);
        });
      }
      if ('country' in metadata[key] && metadata[key].country) {
        countries.add(metadata[key].country);
      }
      if ('PANGO-lineage' in metadata[key] && metadata[key]['PANGO-lineage']) {
        lineages.add(metadata[key]['PANGO-lineage']);
      }
    }
  }
  const selectOptions = {
    id: Object.keys(metadata),
    variants: Array.from(variants).sort(),
    country: Array.from(countries).sort(),
    'PANGO-lineage': Array.from(lineages).sort(),
  };
  return [tableData, selectOptions];
}
