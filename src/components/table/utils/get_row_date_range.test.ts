import { getRecordDateRange } from './get_row_date_range';

test('years', () => {
  const rows = [
    { values: { date: { year: 2019 } } },
    { values: { date: { year: 2018 } } },
    { values: { date: { year: 2020 } } },
    { values: { date: { year: 2019 } } },
  ];
  // eslint-disable-next-line
  expect(getRecordDateRange(rows)).toEqual({ minDate: '2018', maxDate: '2020' });
});

test('years-months', () => {
  const rows = [
    { values: { date: { year: 2019, month: 10 } } },
    { values: { date: { year: 2019, month: 10 } } },
    { values: { date: { year: 2019, month: 1 } } },
    { values: { date: { year: 2018, month: 2 } } },
    { values: { date: { year: 2020, month: 1 } } },
    { values: { date: { year: 2020, month: 9 } } },
    { values: { date: { year: 2019, month: 1 } } },
  ];
  // eslint-disable-next-line
  expect(getRecordDateRange(rows)).toEqual({ minDate: '2018-02', maxDate: '2020-10' });
});
