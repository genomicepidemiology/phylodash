import { Row } from 'react-table';

import { MetadataValue } from '../../../types';
import { TableData } from '../types';
import { compareDateStrings, formatCustomDate } from './custom_date_utils';

export function getRecordDateRange<T extends MetadataValue>(
  preFilteredRows: Row<TableData<T>>[]
): { minDate: string; maxDate: string } {
  const init = {
    minDate: formatCustomDate(preFilteredRows[0].values.date),
    maxDate: formatCustomDate(preFilteredRows[0].values.date),
  };
  const dateRange = preFilteredRows.reduce((acc, row) => {
    const date = formatCustomDate(row.values.date);
    const minDate = compareDateStrings(acc.minDate, date) > 0 ? date : acc.minDate;
    const maxDate = compareDateStrings(date, acc.maxDate) > 0 ? date : acc.maxDate;
    return { minDate, maxDate };
  }, init);
  return dateRange;
}
