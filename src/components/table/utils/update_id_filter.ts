import { Set as iSet } from 'immutable';

export function updateIdFilter(state, idFilterVal: iSet<string>) {
  const isIDFilterActive = state.filters.some(({ id }) => id === 'id');
  if (isIDFilterActive) {
    const idFilterIndex = state.filters.findIndex(({ id }) => id === 'id');
    const newFilters = [...state.filters];
    if (idFilterVal && idFilterVal.size > 0) {
      newFilters[idFilterIndex] = { id: 'id', value: idFilterVal };
      return newFilters;
    }
    newFilters.splice(idFilterIndex, 1);
    return newFilters;
  }
  if (idFilterVal && idFilterVal.size > 0)
    return [...state.filters, { id: 'id', value: idFilterVal }];

  return state.filters;
}
