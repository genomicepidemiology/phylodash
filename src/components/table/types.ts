import { Set as iSet } from 'immutable';
import { Column, TableOptions } from 'react-table';

import { State } from '../../state/init';
import { MetadataValue } from '../../types';

export type TableData<T extends MetadataValue> = { id: string } & T;

export type SelectOptions = {
  id: string[];
  variants?: string[];
};

export type TableProps<T extends MetadataValue> = {
  columnsDef: Column<TableData<T>>[];
  data: TableData<T>[];
  idFilterVal: State['idFilterValue'];
  selectOptions: TableOptions<TableData<T>>['selectOptions'];
  selectedIDs: iSet<string>;
  subtreeBaseLeaf?: string;
  showHelp: boolean;
  actions: TableActions;
};

export type TableControlsActions = {
  onIDChange: (ids: iSet<string>) => void;
  onShowSubtree: (leafID: string) => void;
  onShowOrigTree: () => void;
  onDownloadClick?: (ids: iSet<string>) => void;
};

export type TableActions = {
  toggleRowSelected: (id: string, value: boolean) => void;
  toggleAllRowsSelected: (value: boolean) => void;
  toggleAllVisibleRowRowSelect: (ids: string, value: boolean) => void;
  controls: TableControlsActions;
};
