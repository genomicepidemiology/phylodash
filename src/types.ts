import { Set as iSet } from 'immutable';
import type { PhylogenyTreeRef, UndoRedoMethods } from 'react-phylogeny-tree-gl/types';

export type TreeNodeColors = {
  [id: string]: { fillColour: string } | null;
};

export type ColumnDef = { id: string; [key: string]: unknown };

export type CustomDate = { year: number; month?: number; day?: number };

export type MetadataValue = {
  date: CustomDate;
  latitude: number;
  longitude: number;
  description?: string;
  country?: string;
  region?: string;
  'PANGO-lineage'?: string;
  variants?: string[];
};

export type Metadata<T> = {
  [key: string]: T;
};

export type Colors = { [key: string]: string };

export type PhyloDashPros<T extends MetadataValue> = {
  metadata: Metadata<T>;
  columnsDef: ColumnDef[];
  colors: Colors;
  colorColumn: string;
  newick: string;
  showHelp: boolean;
  mapboxApiAccessToken: string;
  onDownloadClick?: (ids: iSet<string>) => void;
  forwardedRef?: React.Ref<unknown>;
  key?: React.Key;
};

export type PhyloDashRefProps = {
  getTree: TreeRef['getTree'];
  setSelectedIDs: (ids: string[]) => void;
  setSubtreeBaseLeaf: (leafID: string, noLevelsUp: number, minBranchLength: number) => void;
};

export type RefProps = {
  source: string;
  interactive: boolean;
  nodeSize: number;
  haloRadius: number;
  haloWidth: number;
  scalebar: boolean;
  highlightColour: [number, number, number, number];
  rootId: string;
  selectedIds: string[];
  styles: TreeNodeColors;
  leafSubtree: {
    leafID: string;
    noLevels: number;
    minLeafToRootLength: number;
  };
};

export type TreeRef = PhylogenyTreeRef<RefProps, UndoRedoMethods>;