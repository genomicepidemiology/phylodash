# PhyloDash

## Properties

```typescript
type CustomDate = { year: number; month?: number; day?: number };

export type MetadataValue = {
  date: CustomDate;
  latitude: number;
  longitude: number;
  description?: string;
  country?: string;
  region?: string;
  'PANGO-lineage'?: string;
  variants?: string[];
};

export type Metadata<T> = {
  [key: string]: T;
};

type Colors = { [key: string]: string };

export type PhyloDashPros<T> = {
  metadata: Metadata<T>;
  columns: ColumnDef[];
  colors: Colors;
  colorColumn: string;
  newick: string;
  showHelp: boolean;
  mapboxApiAccessToken: string;
  onDownloadClick?: (ids: iSet<string>) => void;
  key?: unknown;
};
```

- `metadata`: object containing metadata where keys are accession ids and values are MetadataValue which must contain CustomDate object under `date` key, `latitude`, `longitude`
- `columns`: column definitions supported by react-table
- `colors`: object with keys which are unique values in current color column in metadata and string of RGB code, e.g.: {Benin: "#0d0887"}
- `colorColumn`:
- `newick` newick string to build tree
- `showHelp` boolean indicated if help should be shown
- `onDownloadClick`:
- `key` in case that newick or metadata change it is necessary to reinitialise state of the component (useReducer). To force react to do state reinitialisation pass new key.

## Actions on components:

### Tree

- click on node - highlight node
- click on node with cmd/ctrl - add/remove node to selection
- click on branch - highlight nodes
- click space - deselect
- view subtree
- redraw original tree

### Map

- click on marker - highlight marker
- click on marker with cmd/ctrl - add/remove node to selection
- click space - deselect

### Table:

- Select row
- Deselect row
- filter
