import { MetadataValue } from './types';

export function getValueForUniqueColor(colorKey: string, data: MetadataValue) {
  if (data) {
    // prettier-ignore
    const value = colorKey === 'year' ? data.date.year
    : colorKey === 'year-month' ? `${data.date.year}-${data.date?.month}`
    : data[colorKey];
    return value?.toString() || null;
  }
  return null;
}
