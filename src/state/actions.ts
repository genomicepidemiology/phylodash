import { Set as iSet } from 'immutable';
import React from 'react';

import { GeoPosition, MarkersLocToIDs } from '../components/map/types';

export type ActionStrings = {
  mapSelect: 'MAP_SELECT_IDS';
  deselect: 'DESELECT';
  treeSelect: 'TREE_SELECT_IDS';
  treeSetWorkingIDs: 'TREE_WORKING_IDS';
};

// type ValueAction<T extends string, V extends unknown> = {
//   type: T,
//   value: V,
// }

// export type MapSelectLoc = ValueAction<ActionStrings["mapSelect"], GeoPosition>
export type MapSelectLoc = {
  type: 'MAP_SELECT_IDS';
  location: GeoPosition;
  locationToIDs: MarkersLocToIDs;
};

export type MapAddSelectLoc = {
  type: 'MAP_ADD_SELECT_IDS';
  location: GeoPosition;
  locationToIDs: MarkersLocToIDs;
};

export type Deselect = {
  type: 'DESELECT';
};

export type TreeSelect = {
  type: 'TREE_SELECT_IDS';
  ids: iSet<string>;
};

export type TreeSetWorkingIDs = {
  type: 'TREE_WORKING_IDS';
  workingIDs: iSet<string>;
  rootId?: string | null;
  keepLeafSubtreeID?: boolean;
};

export type SubtreeBaseLeaf = {
  type: 'SET_SUBTREE_BASE_LEAF';
  leafID: string;
  noLevelsUp?: number;
  minBranchLength?: number;
};

export type TableFilterIDs = {
  type: 'FILTER_IDS';
  ids: iSet<string>;
};

export type SamplesFromURL = {
  type: 'SET_IDS_FROM_URL';
  value: string[];
};

export type RowSelect = {
  type: 'ROW_SELECT';
  id: string;
  value?: boolean;
};

export type TableToggleSelectAllVisible = {
  type: 'TABLE_TOGGLE_ALL_VISIBLE';
  ids: string[];
  value: boolean;
};

export type TableToggleSelectAll = {
  type: 'TABLE_TOGGLE_ALL';
  value?: boolean;
};

export type Actions =
  | MapSelectLoc
  | MapAddSelectLoc
  | Deselect
  | TreeSelect
  | TreeSetWorkingIDs
  | SubtreeBaseLeaf
  | TableFilterIDs
  | SamplesFromURL
  | RowSelect
  | TableToggleSelectAll
  | TableToggleSelectAllVisible;

export function useActionCallbacks(
  dispatch: React.Dispatch<Actions>,
  onDownloadClick?: (ids: iSet<string>) => void
) {
  return React.useMemo(() => {
    const deselect = () => {
      dispatch({ type: 'DESELECT' });
    };
    return {
      tree: {
        setSelectedIDs: (ids) => {
          dispatch({ type: 'TREE_SELECT_IDS', ids: ids });
        },
        deselectIDs: deselect,
        setWorkingIDs: (workingIDs, rootId: string | null = null, keepLeafSubtreeID?: boolean) => {
          dispatch({
            type: 'TREE_WORKING_IDS',
            workingIDs: workingIDs,
            keepLeafSubtreeID,
            rootId,
          });
        },
      },
      table: {
        toggleRowSelected: (id, value) => {
          dispatch({ type: 'ROW_SELECT', id, value });
        },
        toggleAllRowsSelected: (value) => {
          dispatch({ type: 'TABLE_TOGGLE_ALL', value });
        },
        toggleAllVisibleRowRowSelect: (ids, value) => {
          dispatch({ type: 'TABLE_TOGGLE_ALL_VISIBLE', ids, value });
        },
        controls: {
          onIDChange: (ids: iSet<string>) => {
            dispatch({ type: 'FILTER_IDS', ids });
          },
          onShowSubtree: (leafID) => {
            dispatch({ type: 'SET_SUBTREE_BASE_LEAF', leafID });
          },
          onShowOrigTree: () => {
            dispatch({
              type: 'TREE_WORKING_IDS',
              workingIDs: iSet(),
            });
          },
          onDownloadClick,
        },
      },
      map: {
        deselect: deselect,
        // setSelected: (location, metaOrCtrl) => {
        //   if (metaOrCtrl) dispatch({ type: 'MAP_ADD_SELECT_IDS', location });
        //   else dispatch({ type: 'MAP_SELECT_IDS', location });
        // }
      },
    };
  }, [dispatch, onDownloadClick]);
}
