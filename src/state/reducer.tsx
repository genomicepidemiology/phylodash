import { Set as iSet, List as iList } from 'immutable';

import { HighlightedLoc, GeoPosition } from '../components/map/types';
import { TreeLabelMappers } from '../components/tree/hooks/useTreeMappers';
import { Metadata, MetadataValue } from '../types';
import { Actions } from './actions';
import { State } from './init';

export default function createReducer<T extends MetadataValue>(
  props: { metadata: Metadata<T> },
  treeIdMappers: TreeLabelMappers
) {
  return (state: State, action: Actions): State => {
    switch (action.type) {
      case 'MAP_ADD_SELECT_IDS': {
        // eslint-disable-next-line no-console
        console.log('MAP_ADD_SELECT_IDS');

        const ids = action.locationToIDs.get(action.location);
        const [markerHighlightedLoc, highlightedIDs] = state.markerHighlightedLoc.has(
          action.location
        )
          ? [state.markerHighlightedLoc.delete(action.location), state.selectedIDs.subtract(ids)]
          : [state.markerHighlightedLoc.add(action.location), state.selectedIDs.union(ids)];

        return {
          ...state,
          markerHighlightedLoc,
          idFilterValue: highlightedIDs,
          selectedIDs: highlightedIDs,
        };
      }
      case 'MAP_SELECT_IDS': {
        // eslint-disable-next-line no-console
        console.log('MAP_SELECT_IDS');
        const markerHighlightedLoc = iSet<GeoPosition>().add(action.location);
        const highlightedIDs = action.locationToIDs.get(action.location);

        return {
          ...state,
          markerHighlightedLoc,
          idFilterValue: highlightedIDs,
          selectedIDs: highlightedIDs,
        };
      }
      case 'DESELECT': {
        // eslint-disable-next-line no-console
        console.log('DESELECT');
        const markerHighlightedLoc = state.markerHighlightedLoc.clear();
        return {
          ...state,
          markerHighlightedLoc,
          selectedIDs: iSet(),
          idFilterValue: iSet(),
        };
      }
      case 'SET_IDS_FROM_URL': {
        // eslint-disable-next-line no-console
        console.log('SET_IDS_FROM_URL');
        const ids = iSet(action.value);

        const markerHighlightedLoc = getHighlightedLocFromIDs(action.value, props.metadata);
        return {
          ...state,
          markerHighlightedLoc,
          idFilterValue: ids,
          selectedIDs: ids,
        };
      }
      case 'TREE_SELECT_IDS': {
        // eslint-disable-next-line no-console
        console.log('TREE_SELECT_IDS');

        const markerHighlightedLoc = getHighlightedLocFromIDs(action.ids, props.metadata);
        const selectedIDs = iSet(action.ids);
        return {
          ...state,
          markerHighlightedLoc,
          idFilterValue: selectedIDs,
          selectedIDs: selectedIDs,
        };
      }
      case 'TREE_WORKING_IDS': {
        // eslint-disable-next-line no-console
        console.log('TREE_WORKING_IDS');
        const shownIDs = action.workingIDs;
        if (state.selectedIDs.every((id) => shownIDs.has(id))) {
          return {
            ...state,
            shownIDs,
            rootId: action.rootId,
            subtreeBaseLeafLabel: action.keepLeafSubtreeID ? state.subtreeBaseLeafLabel : undefined,
          };
        }
        const selectedIDs = state.selectedIDs.intersect(shownIDs);
        const markerHighlightedLoc = getHighlightedLocFromIDs(
          selectedIDs.toArray(),
          props.metadata
        );
        return {
          ...state,
          selectedIDs,
          idFilterValue: selectedIDs,
          markerHighlightedLoc,
          shownIDs,
          rootId: action.rootId,
          subtreeBaseLeafLabel: action.keepLeafSubtreeID ? state.subtreeBaseLeafLabel : undefined,
        };
      }
      case 'SET_SUBTREE_BASE_LEAF': {
        // eslint-disable-next-line no-console
        console.log('SET_SUBTREE_BASE_LEAF');
        const subtreeBaseLeafLabel = treeIdMappers.accessionToLeaf.get(action.leafID);
        const ids = iSet([action.leafID]);
        const markerHighlightedLoc = getHighlightedLocFromIDs([action.leafID], props.metadata);

        return {
          ...state,
          markerHighlightedLoc,
          idFilterValue: ids,
          selectedIDs: ids,
          subtreeBaseLeafLabel,
          noLevelsUp: action.noLevelsUp,
          minBranchLength: action.minBranchLength,
        };
      }
      case 'ROW_SELECT': {
        // eslint-disable-next-line no-console
        console.log('ROW_SELECT');
        const selectedIDs = state.selectedIDs.has(action.id)
          ? state.selectedIDs.remove(action.id)
          : state.selectedIDs.add(action.id);

        const markerHighlightedLoc = getHighlightedLocFromIDs(selectedIDs, props.metadata);

        return {
          ...state,
          selectedIDs,
          markerHighlightedLoc,
        };
      }
      case 'TABLE_TOGGLE_ALL': {
        // eslint-disable-next-line no-console
        console.log('TABLE_TOGGLE_ALL');
        const allIDs = Object.keys(props.metadata);

        // prettier-ignore
        const selectedIDs: iSet<string> =
          action.value === undefined && state.selectedIDs.size < allIDs.length
          ? iSet(allIDs)
          : action.value === true ? iSet(allIDs)
          : iSet();

        const markerHighlightedLoc = getHighlightedLocFromIDs(selectedIDs, props.metadata);

        return {
          ...state,
          selectedIDs,
          markerHighlightedLoc,
        };
      }
      case 'TABLE_TOGGLE_ALL_VISIBLE': {
        // eslint-disable-next-line no-console
        console.log('TABLE_TOGGLE_ALL_VISIBLE');
        // prettier-ignore
        const [selectedIDs, markerHighlightedLoc] =
          action.value === undefined && state.selectedIDs.size < action.ids.length
              ? [iSet<string>(action.ids), getHighlightedLocFromIDs(action.ids, props.metadata)]
            : action.value === true ? [iSet(action.ids), getHighlightedLocFromIDs(action.ids, props.metadata)]
            : [iSet<string>(), iSet<GeoPosition>()];

        return {
          ...state,
          selectedIDs,
          markerHighlightedLoc,
        };
      }
      case 'FILTER_IDS': {
        // eslint-disable-next-line no-console
        console.log('FILTER_IDS');
        if (!action.ids)
          return {
            ...state,
            idFilterValue: iSet(),
          };

        return {
          ...state,
          idFilterValue: action.ids,
        };
      }
      default:
        neverReached(action);
    }
  };
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const neverReached = (never: never) => {
  return;
};

function getHighlightedLocFromIDs<T extends MetadataValue>(
  ids: iSet<string> | string[],
  metadata: Metadata<T>
): HighlightedLoc {
  const highlightedLoc = iSet<GeoPosition>().withMutations((set) => {
    ids.forEach((id: string) => {
      if (!(id && Object.prototype.hasOwnProperty.call(metadata, id))) return;
      const lat = metadata[id].latitude;
      const lgn = metadata[id].longitude;
      set.add(iList([lat, lgn]));
    });
  });
  return highlightedLoc;
}
