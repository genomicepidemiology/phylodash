import { Set as iSet } from 'immutable';

import { HighlightedLoc, GeoPosition } from '../components/map/types';
import { Metadata } from '../types';

export type State = {
  rootId?: string;
  noLevelsUp?: number;
  minBranchLength?: number;
  subtreeBaseLeafLabel?: string;
  markerHighlightedLoc: HighlightedLoc;
  selectedIDs: iSet<string>;
  shownIDs: iSet<string>;
  idFilterValue: iSet<string>;
};

export function initState<T>(metadata: Metadata<T>): State {
  return {
    rootId: null,
    subtreeBaseLeafLabel: undefined,
    noLevelsUp: undefined,
    minBranchLength: undefined,
    markerHighlightedLoc: iSet<GeoPosition>(),
    selectedIDs: iSet<string>(),
    shownIDs: iSet(Object.keys(metadata)),
    idFilterValue: undefined,
  };
}
